package Lections.Lection8;

public class Task1 {

    public static void main(String[] args) {
        /*  Необходимо написать функцию, подсчитывающую сумму всех элементов множества массивов. Кол-во массивов и их размер неопределены.
         */
        Task1 arrays = new Task1();
        arrays.run();

    }


    void run() {
        int elementCount = 10;
        Math.pow(1, 2);

        int[] zweiPowerarray = generateArray(elementCount, 2);
        int[] dreiPowerarray = generateArray(elementCount, 3);
        int[] vierPowerarray = generateArray(elementCount, 4);


        System.out.println("Sum is " + getSum(zweiPowerarray, dreiPowerarray, vierPowerarray));
    }

    private int getSum(int[]... arrays) {
        int sum = 0;
        for (int[] array : arrays) {
            sum += getSumOfArray(array);
        }

        return sum;
    }

    private int getSumOfArray(int[] array) {
        int sum = 0;
        for (int element : array) {
            sum += element;
        }
        return sum;
    }

    private int[] generateArray(int elementCount, int i) {
        int[] array = new int[elementCount];
        for (int n = 0; n < elementCount; ++n) {
            array[n] = (int) Math.pow(n+1, i);


        }
        return array;

    }


}