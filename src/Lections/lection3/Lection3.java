package Lections.lection3;

public class Lection3 {
    // Объявите поля всех примитивных типов и проинициализируйте их
    double variable = 5.6;

    public void run() {
        double variable = 5.5;
        double result = this.variable;
    }

    public static void main(String[] args) {
        Lection3 main = new Lection3();
        main.run();
        float variable = 3.5F;
    }
}

