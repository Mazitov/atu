package Lections.lection14;

import Lections.lection14.work.*;


public class TestProgramm {

    /*Создайте пакет lecture 14.
В нем класс TestProgramm, у которого есть метод main(String[] args) и создайте еще пакет “work”, в котором создайте классы Company и Employee.

Для Company добавить: name, address, sphere, сотрудники (массив из Employee)
Employee: name, position

Добавить в классах Company и Employee геттеры и сеттеры по необходимости (например, можно не добавлять сеттер для name в классе Employee, т.к. имя у сотрудника не меняется).

В классе Company написать вспомогательный метод (должен быть доступен только в самом классе), чтобы по должности была возможность найти сотрудника (предположим, что в наших компаниях должности не повторяются).
//на занятии мы добавили:
•  метод чтобы получить владельца компании (эта информация должны быть доступна только в классах-наследниках)
•  общедоступный метод, чтобы узнать кто руководит компанией (назвать можно и генеральным директор, и CEO, как вам удобнее).
Использовать новый вспомогательный метод в выше перечисленных методах.

Для Employee добавить поле salary.
Добавить возможность получить максимальную и среднюю зарплату в компании.
Добавьте 3 компании («T-Systems», «GetBrains», «Dell») с произвольным набором сотрудником (в каждой компании он различный).
Вывести на экран максимальную и среднюю зарплату во всех компаниях.*/


    public static void main(String[] args) {

        String[] randomAddresses = {"Moscow", "Chelyabinsk", "Krasnodar", "St Petersburg", "Kazan", "Orel", "Volgograd", "Ekaterinburg", "Omsk", "Vladivostok"};

        Company tSystems = new Company("T-Systems", randomAddresses[(int) Math.round(Math.random() * 9)], createEmployeeArray());
        Company jetBrains = new Company("JetBrains", randomAddresses[(int) Math.round(Math.random() * 9)], createEmployeeArray());
        Company dell = new Company("Dell", randomAddresses[(int) Math.round(Math.random() * 9)], createEmployeeArray());

        printCompanyStatistic(tSystems);
        printCompanyStatistic(jetBrains);
        printCompanyStatistic(dell);

        System.out.println(tSystems.findEmployee("developer").getName() + " " + tSystems.findEmployee("developer").getPosition());
    }


    public static Employee[] createEmployeeArray() {

        String[] randomNames = {"Anna", "Sergey", "Ivan", "Stepan", "Inga", "Olga", "Alexey", "Svetlana", "Dmitry", "Anton"};
        String[] randomPosition = {"developer", "manager", "tester", "configurator", "devops", "accounter",};

        Employee[] employeesArray = new Employee[(int) (Math.round(Math.random() * 15) + 1)];
        for (int i = 0; i < employeesArray.length; ++i) {
            employeesArray[i] = new Employee((randomNames[(int) Math.round(Math.random() * 9)]), (randomPosition[(int) Math.round(Math.random() * 4)]));
            int salaryGenerator = (int) ((Math.random() * 10) + 1) * 10000;
            employeesArray[i].setSalary(salaryGenerator);
        }
        return employeesArray;
    }

    public static void printCompanyStatistic(Company company) {
        System.out.println("Company: " + company.getName() + "\n" + company.printCollectionOfEmployees());
        System.out.println("Maximal salary in " + company.getName() + " is " + company.maxSalary() + ".");
        System.out.println("Average salary in " + company.getName() + " is " + company.avgSalary() + ".");
        System.out.println("_______________________________\n");
    }

}
