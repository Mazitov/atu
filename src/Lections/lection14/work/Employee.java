package Lections.lection14.work;

public class Employee {
    private String name;
    private String position;


    private int salary;

    public Employee(String name, String position) {
        this.name = name;
        this.position = position;
        salary = 50000;
    }

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        if (salary > 0 ){
        this.salary = salary;}
        else {
            System.out.println("Salary cannot be set to 0");
        }
    }


}
