package Lections.lection14.work;

public class Company {

    private String name, address, sphere;
    private Employee[] collectionOfEmployees;
    private String owner;
    private String director;


    public Company(String name, String address, Employee[] collectionOfEmployees) {
        this.name = name;
        this.address = address;
        this.collectionOfEmployees = collectionOfEmployees;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    protected String getOwner() {
        return owner;
    }

    public String getDirector() {
        return director;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSphere() {
        return sphere;
    }

    public void setSphere(String sphere) {
        this.sphere = sphere;
    }

    public String printCollectionOfEmployees() {
        String c = "";
        for (Employee a : collectionOfEmployees) {
            c += a.getName() + " " + a.getPosition() + "\n";
        }
        return c;
    }


    public int maxSalary() {
        int maximum = 0;
        for (Employee emp : collectionOfEmployees) {
            if (maximum < emp.getSalary()) maximum = emp.getSalary();
        }
        return maximum;
    }


    public int avgSalary() {
        int average = 0;
        for (Employee emp : collectionOfEmployees) {
            average += emp.getSalary();
        }
        average /= collectionOfEmployees.length;
        return average;
    }

    public Employee findEmployee(String position) {
        Employee empl = new Employee("not found", "not found");
        for (Employee employee : collectionOfEmployees) {
            if (position == employee.getPosition()) {
                return employee;
            }
        }
        return empl;
    }

}
