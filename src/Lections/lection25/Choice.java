package Lections.lection25;

import com.sun.source.util.TaskListener;

public class Choice implements Task {
    String[] answer;

    public Choice(String[] answer) {
        this.answer = answer;
    }

    @Override
    public int getScore() {
        int sum = 3;
        for (String i : answer) {
            if (i == null) --sum;
        }
        return sum;
    }
}
