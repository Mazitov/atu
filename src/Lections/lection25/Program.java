package Lections.lection25;

public class Program implements Task {
    String answer;

    public Program(String answer) {
        this.answer = answer;
    }

    public int getScore() {
        char[] answerChar = answer.toCharArray();
        if (answerChar.length >= 100) {
            return 10;
        } else {
            return answerChar.length / 10;
        }
    }
}
