package Lections.lection25;

public class Exam {
    private Task[] tasksLists;

    public Exam(Task[] tasksLists) {
        this.tasksLists = tasksLists;
    }

    public int getScore() {
        int sum = 0;
        for (Task a : tasksLists) {
            sum += a.getScore();
        }
        return sum;
    }
}
