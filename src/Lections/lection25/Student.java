package Lections.lection25;

public class Student {
    private final String name;
    private Exam exam;

    public Student(String name, Exam exam) {
        this.name = name;
        this.exam = exam;
    }

    public void getScore() {
        if (exam.getScore() > 30) {
            System.out.println("Студент " + name + " набрал " + exam.getScore() +" баллов. И успешно сдал экзамен.");
        } else  {
            System.out.println("Студент " + name + " набрал " + exam.getScore() +" баллов. И не сдал экзамен.");

        }


    }
}
