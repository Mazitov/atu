package Lections.Lection4;

public class Relation {
    public static void main(String args[]) {
        int a = 1;
        int b = 1;
        System.out.println("a == " + a + ", b == " + b);
        System.out.println("a == b: " + (a == b));
        System.out.println("a != b: " + (a != b));

        System.out.println("a >= b: " + (a >= b));
        System.out.println("a > b: " + (a > b));

        System.out.println("1 == 1.0: " + (1 == 1.0));
        System.out.println("1 == 1.01: " + (1 == 1.01));
    }
}
