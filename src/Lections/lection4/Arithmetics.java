package Lections.Lection4;

public class Arithmetics {

    public static void main(String args[]) {
        // наиболее простые опрации
        int i = 1;
        int v = 5;
        int x = 10;
        int l = 50;
        int c = 100;

        int a = 0;
        int b = 0;

        System.out.println("Operation 1: " + (i + v));

        a += x;
        System.out.println("Operation 2: " + a);
        System.out.println("Operation 3: " + (b += l)); // так можно, но не рекомендуется, так как сложно воспринимается
        System.out.println("Operation 4: " + (c % 3)); // остаток от деления на 3
    }
}


