package Lections.lection4;

public class Increments {
    public static void main(String args[]) {

        int a = 1;
        int b = 1;
        a++;
        System.out.println("Operation 1: " + a);
        ++b;
        System.out.println("Operation 2: " + b);

        a = 1;
        System.out.println("Operation 3: " + a++); // результат - исходное значение переменной
        System.out.println("Operation 4: " + ++a); // а тут больше на 2

        // вопрос - чему сейчас равно значение выражения a++ + ++a
        System.out.println("Operation 5: " + (a++ + ++a));

        double d = 3.14;
        System.out.println("Operation 6: " + --d); // с плавающей запятой тоже работает

    }
}
