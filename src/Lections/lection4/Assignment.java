package Lections.Lection4;

public class Assignment {
    public static void main(String args[]) {
        int i = 0;
        System.out.println("Assignment test: " + i);
        i = 2; // возвращает ли это выражение что-то?
        System.out.println("Assignment test2: " + (i = 2)); // да, возвращает результат операции

        String s;
        System.out.println("Assignment test3: " + (s = "something")); // да, так тоже работает

        byte byte1 = 1;
        // byte1 = byte1 + 50; - тут ошибка, так как 50 имеет тип int по умолчанию, и результат имеет тип int

        // сужающее преобразование
        byte byte2 = (byte)514;

        // немного магии следующего уровня, для наглядности
        System.out.println(String.format("По битам 514: %s", Integer.toBinaryString(514)));
        System.out.println("Значение byte2: " + byte2);
        System.out.println(String.format("По битам byte2: %s", Integer.toBinaryString(byte2)));
    }
}
