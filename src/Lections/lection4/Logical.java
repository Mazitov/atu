package Lections.Lection4;

public class Logical {
    public static void main(String args[]) {

        System.out.println("true & false: " + (true & false));
        System.out.println("true | false: " + (true | false));
        System.out.println("true ^ false: " + (true ^ false));

        System.out.println("true && false: " + (true && false));
        System.out.println("true || false: " + (true || false));

        // результатом логических операций является значение типа boolean
        boolean result = 100 > 500;
        boolean trueVar = true;
        boolean falseVar = false;

        System.out.println("result: " + result);

        result &= falseVar;
        System.out.println("result & false: " + result);

        result |= trueVar;
        System.out.println("result | true: " + result);

        result ^= trueVar;
        System.out.println("result ^ true: " + result);

        // попробуем еще раз сделать xor:
        result ^= trueVar;
        System.out.println("result ^ true: " + result);

        System.out.println("result != true (не равно): " + (result != true));

        int a = 2;
        System.out.println("Логический оператор & выполныят оба операнда: " + (a != 2 & (100 / a++) > 0));
        System.out.println("Значение а: " + a);
        a = 2;
        System.out.println("Условный оператор && прекращает выполнение после первой проверки, так как она ложна: "
                + (a != 2 && (100 / a++) > 0));
        System.out.println("Значение а: " + a);
        // можно писать выражения типа а > 0 && 100 / a, это безопасно, деления на 0 не будет

        System.out.println("!false: " + !false);
        System.out.println("!true: " + !true);

        System.out.println("a > b: " + (a > 0));
        System.out.println("!(a > b): " + !(a > 0));
    }
}
