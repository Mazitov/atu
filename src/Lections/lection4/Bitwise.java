package Lections.Lection4;

public class Bitwise {
    public static void main(String args[]) {
        int a = 0b1010;
        int b = 0b1100;

        System.out.println("Побитовое ИЛИ " + (a | b));
        System.out.println("Побитовое И " + (a & b));
        System.out.println("Побитовое ИСКЛЮЧАЮЩЕЕ ИЛИ (xor) " + (a ^ b));
        System.out.println("Побитовое отрицание (инверсия) " + ~a); // что??

        // немного магии следующего уровня, для наглядности. Мы инвертировали целый int, 32 бита
        System.out.println(String.format("По битам: %s", Integer.toBinaryString(~a)));

        System.out.println(String.format("1 << 1: %32s", Integer.toBinaryString(1 << 1)));
        System.out.println(String.format("0 << 1: %32s", Integer.toBinaryString(0 << 1)));
        System.out.println(String.format("0 >> 1: %32s", Integer.toBinaryString(0 >> 1)));
        System.out.println(String.format("1 >> 2: %32s", Integer.toBinaryString(1 >> 2)));

        System.out.println(String.format("101 << 3: %32s", Integer.toBinaryString(0b101 << 3)));
        System.out.println(String.format("101 >> 2: %32s", Integer.toBinaryString(0b101 >> 2)));

        System.out.println(String.format("-32      : %32s", Integer.toBinaryString(-32)));
        System.out.println(String.format("-32 >> 5 : %32s", Integer.toBinaryString(-32 >> 5)));
        System.out.println(String.format("-32 >>> 5: %32s", Integer.toBinaryString(-32 >>> 5)));

        System.out.println( "Значение -32 >> 5: (деление на 2^5) "+ (-32 >> 5));
    }
}
