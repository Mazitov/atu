package Lections.lection9;

public class Series {
    public static int iMax = 13;

    public static void main(String[] args) {
        Series series = new Series();
        series.run();
    }

    private void run() {
        System.out.println(calc(iMax));
    }

    public int calc(int iMax) {
        if (iMax == 1) {
            return 1;
        } else {
             if (iMax < 0) {
                return calc(-(iMax + 1));
            } else {
                return calc(-(iMax - 1));
            }

        }
    }
}
    /*

    Посчитать сумму ряда рекурсивным способом
    Ряд такой: 1 -2 3 -4 5 -6 7 -8 9 -10

     */



 /*   public int calc(int a) {
        for (int i = 0; i < iMax2 - 1; ++i) {
            if (iMax2 == 0) return 0;
            if (a > 0) {
                a++;
                a = -a;
            } else if (a < 0) {
                --a;
                a = -a;
            }
        }
        return a;
    }*/