package Lections.lection9;

public class Calculator {
    public static int ai = 1, bi = 5;
    public static double ad = 1d, bd = 5d;
    public static long aL = 1000L, bL = 5000L;


    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.run();
    }

    private void run() {
        printall(ai, bi, ad, bd, aL, bL);
        System.out.println("int a + int b = " + plus(ai, bi));
        System.out.println("int a - int b = " + minus(ai, bi));
        System.out.println("int a * int b = " + umnozh(ai, bi));
        System.out.println("int a / int b = " + delim(ai, bi));
        System.out.println("double a + double b = " + plus(ad, bd));
        System.out.println("double a - double b = " + minus(ad, bd));
        System.out.println("double a * double b = " + umnozh(ad, bd));
        System.out.println("double a / double b = " + delim(ad, bd));
        System.out.println("long a + long b = " + plus(aL, bL));
        System.out.println("long a - long b = " + minus(aL, bL));
        System.out.println("long a * long b = " + umnozh(aL, bL));
        System.out.println("long a / long b = " + delim(aL, bL));
    }

    private void printall(int ai, int bi, double ad, double bd, long aL, long bL) {
        System.out.println(" int a = " + ai + "\n int b = " + bi + "\n double a = " + ad + "\n double b = " + bd + "\n long a = " + aL + "\n long b = " + bL);
    }

    private long delim(long aL, long bL) {
        return (aL / bL);
    }

    private long umnozh(long aL, long bL) {
        return (aL * bL);
    }

    private long minus(long aL, long bL) {
        return (aL - bL);
    }

    private long plus(long aL, long bL) {
        return (aL + bL);
    }


    private double delim(double ad, double bd) {
        return (ad / bd);
    }

    private double umnozh(double ad, double bd) {
        return (ad * bd);
    }

    private double minus(double ad, double bd) {
        return (ad - bd);
    }

    private double plus(double ad, double bd) {
        return (ad + bd);
    }


    private int delim(int ai, int bi) {
        return (ai / bi);
    }

    private int umnozh(int ai, int bi) {
        return (ai * bi);
    }

    private int minus(int ai, int bi) {
        return (ai - bi);
    }

    private int plus(int ai, int bi) {
        return (ai + bi);
    }

    /*

    Для каждой операции + - * /
    Напишите функцию возвращающую результат этот операции для 2-ух чисел
    Числа могут быть int double long

    в run() посчитайте с помощью этих функций выражения
    a + b, a * b, a / b, a - b
    для разных типов a и b

    Расширьте программу чтобы она позволяла делать операции с любым кол-вом чисел.

     */
}
