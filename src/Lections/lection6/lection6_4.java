package Lections.Lection6;

public class lection6_4 {
    public static void main(String[] args) {
        //используя расширенный цикл for вывести на жкран все элементы массива, предварительно увеличив их на 1
        int[] hello = {1, 2, 3, 4, 5, 0, -5};

        for (int element : hello) {
            System.out.print(++element + " ");
        }

    }
}
