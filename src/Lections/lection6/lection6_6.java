package Lections.lection6;

public class lection6_6 {


    public static void main(String[] args) {
        //вывести на экран все элементы массива, используя цикл for и расширенный цикл for
        double[] hello = {1.2, 22, 3, 4, 5, 0.5, -5};


        for (double element : hello) {
            System.out.print(element + " ");
        }

        System.out.println();

        for (int i = 0; i < hello.length; ++i) {
            System.out.print(hello[i] + " ");
        }


    }


}
