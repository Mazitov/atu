package  Lections.lection16;

public class Singleton {

    public static void main (String args []) {
        String login = Singleton.getInstance().getLogin();
        String password = Singleton.getInstance().getPassword();
    }

    private final String userName;
    private final String password;

    private static Singleton instance = new Singleton();

    // конструктор объявлен private, снаружи сконструировать объет нельзя
    private Singleton() {
        // здесь код конструктора
        // например, он считывает из файла различные настройки
        // опустим сам код чтения из файла
        userName = "user11";
        password = "S3cr33t";
    }

    // для получения доступа к классу необходимо вызвать публичный метод getInstance()
    public static Singleton getInstance() {
        // в результате, все, кто захочет воспользоваться классом,
        // получит ссылку на один и тот же объект
        return instance;
    }

    public String getLogin() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
}
