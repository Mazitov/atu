package  Lections.lection16.factory;


public class Application {
    public static void main (String args []) {
        CatRation kittenKation = CatRation.createCatRationInsance(CatFoodFactory.KITTEN_FOOD_TYPE);
        CatRation matureRation = CatRation.createCatRationInsance(CatFoodFactory.MATURE_FOOD_TYPE);
        kittenKation.printRation();
        matureRation.printRation();
    }
}
