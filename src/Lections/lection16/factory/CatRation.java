package  Lections.lection16.factory;

public class CatRation {
    private int dryMealGramms;
    private int freshMealGramms;
    private int vitaminTablets;

    public static final String KITTEN_FOOD_TYPE = "kitten";
    public static final String MATURE_FOOD_TYPE = "mature";

    // Обычно в таких случаях используется Enum - перечислимый тип,
    // Но про работу с ним мы расскажем в третьем модуле
    // Обойдемся строками
    public static CatRation createCatRationInsance(String type) {
        // здесь сложные правила конструирования объектов
        if (KITTEN_FOOD_TYPE.equals(type)) {
            return new CatRation(80,150,3);
        }
        else if (MATURE_FOOD_TYPE.equals(type)) {
            return new CatRation(120,180,3);
        }
        else {
            System.out.println("Неизвестный тип котика!");
            // тут стоит бросить исключение, но мы их еще не проходили. Просто вернем null
            return null;
        }
    }

    CatRation(int dry, int fresh, int vitamin) {
        dryMealGramms = dry;
        freshMealGramms = fresh;
        vitaminTablets = vitamin;
    }

    public void printRation() {
        System.out.println("Котик будет есть сухого корма: " + dryMealGramms + " грамм");
        System.out.println("Котик будет есть свежего корма: " + freshMealGramms + " грамм");
        System.out.println("Котик будет есть витамины: " + vitaminTablets + " таблеток\n");
    }
}
