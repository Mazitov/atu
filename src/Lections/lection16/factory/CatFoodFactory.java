package  Lections.lection16.factory;

// Фабрика - класс, позволяющй создать другой класс. В ней находится логика по созданию объектов,
// которая не относится к создаваемому классу, и поэтому ее не хочется помещать в конструктор.
// Так, например, сведения о котиках и их возможном возрасте не относятся к их еде
public class CatFoodFactory {
    public static final String KITTEN_FOOD_TYPE = "kitten";
    public static final String MATURE_FOOD_TYPE = "mature";

    // Обычно в таких случаях используется Enum - перечислимый тип,
    // Но про работу с ним мы расскажем в третьем модуле
    // Обойдемся строками
    public static CatRation createCatRationInsance(String type) {
        // здесь сложные правила конструирования объектов
        if (KITTEN_FOOD_TYPE.equals(type)) {
            return new CatRation(80,150,3);
        }
        else if (MATURE_FOOD_TYPE.equals(type)) {
            return new CatRation(120,180,3);
        }
        else {
            System.out.println("Неизвестный тип котика!");
            // тут стоит бросить исключение, но мы их еще не проходили. Просто вернем null
            return null;
        }
    }
}
