package  Lections.lection16.static_field_example;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class StaticCat {
    // Обычно здесь помещают "константы уровня класса"
    // Какая-либо информация, нужная всем классам
    public static final String LAST_CAT_CREATION_STRING = "Последний котик был создан: ";
    public static final String LAST_CAT_NAME_STRING = "Последнего котика звали: ";
    public static final String NO_CATS_NOTIFIER_STRING = "Котиков еще не было";

    private static LocalDateTime lastCreationTimestamp;
    private static int instancesCreated = 0;
    private static String lastCatName;

    private final String name;
    private double age;
    private double weight;

    // подобно обычному блоку инициализации, можно объявить статический блок инициализации
    // в нем можно инициализировать статические поля. Понятно, что к нестатическим полям нет доступа
    // блоков может быть несколько, они выполняются в порядке объявления
    static {
        // мы инициализируем это поле в блоке только для примера,
        // его вполне можно было проинициализировать и при объявлении
        lastCatName = "";
    }

    StaticCat(String name, double age, double weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        // now - статический метод класса LocalDateTime
        lastCreationTimestamp = LocalDateTime.now(ZoneId.systemDefault());
        ++instancesCreated;
        lastCatName = name;
    }

    public static void printLastInstanceCreationTimestamp() {
        if (lastCreationTimestamp != null) {
            System.out.println(LAST_CAT_CREATION_STRING + lastCreationTimestamp);
        }
        else {
            System.out.println(NO_CATS_NOTIFIER_STRING);
        }
    }

    public static int getInstancesCreated() {
        return instancesCreated;
    }

    public static void printLastCatName() {
        if (lastCatName.length() > 0) {
            System.out.println(LAST_CAT_NAME_STRING + lastCatName);
        }
        else {
            System.out.println(NO_CATS_NOTIFIER_STRING);
        }
    }
}
