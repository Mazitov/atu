package  Lections.lection16.static_field_example;

public class Application {
    public static void main (String args []) {
        StaticCat.printLastInstanceCreationTimestamp();
        StaticCat.printLastCatName();
        String noCatsNotifierString = StaticCat.NO_CATS_NOTIFIER_STRING;

        StaticCat begemoth = new StaticCat("Begemoth", 1000, 41.0);
        StaticCat.printLastInstanceCreationTimestamp();
        StaticCat.printLastCatName();

        StaticCat pixel = new StaticCat("Pixel", 0.1, 0.25);
        StaticCat.printLastInstanceCreationTimestamp();
        StaticCat.printLastCatName();

        StaticCat margo = new StaticCat("Margo", 2, 3.2);
        StaticCat.printLastInstanceCreationTimestamp();
        StaticCat.printLastCatName();

        System.out.println("Всего было создано котиков: " + StaticCat.getInstancesCreated());
    }
}

