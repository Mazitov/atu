package  Lections.lection16;

import java.lang.Math;


import static java.lang.Math.pow;
import static java.lang.Math.sqrt;


public class StaticImport {
    public static void main (String args []) {
        double side1, side2;
        double hypot;
        side1 = 3.0;
        side2 = 4.0;
        // Обратите внимание на то , что имена методов sqrt () и pow()
        // должны быть уточнены именем их класса Мath
        hypot = Math.sqrt(Math.pow(side1, 2) + Math.pow(side2, 2));
        System.out.println("Пpи заданной длине сторон " + side1 + " и " + side2 + " гипотенуза равна " + hypot);

        // Так методы sqrt() и pow () можно вызывать непосредственно,
        // опуская имя их класса
        // обращаем внимание на импорты вверху
        hypot = sqrt(pow(side1, 2) + pow(side2, 2));
    }
}
