package Lections.lection16.access_static;

public class AccessToStaticMembers {
    // Иллюстрация доступа к статическим и нестатическим членам класса извне класса

    public static int publicStaticField1 = 1;
    private static int privateStaticField = 2;
    public int publicField = 3;

    // инициализация в блоке static возможна только для статических членов
    static {
        publicStaticField1 = 10000;
        privateStaticField = 20000;

        // вызов статических методов возможен
        publicStaticMethod1();
        privateStaticMethod2();

        // доступ к нестатическому члену невозможен
        // publicField = 30000;

        // вызов нестатического метода невозможен
        // method1();
    }

    AccessToStaticMembers() {
        // доступ ко всем полям возможен
        // это инициализация нестатического поля
        publicField = 1000;

        // доступ к статическим полям возможен,
        // изнутри класса модификаторы доступа не ограничиват доступ
        // однако, модификация статических полей внутри конструктора обычно бесполезна
        publicStaticField1 = 1500;
        privateStaticField = 2000;

        // доступ к статическим методам возможен
        // изнутри класса модификаторы доступа не ограничиват доступ
        publicStaticMethod1();
        privateStaticMethod2();

        // доступ к нестатическому методу из коструктора возможен,
        // в обзем случае не рекомендуется,
        // так как объект еще не до конца сконструирован
        method1();
    }

    public void method1() {
        System.out.println("Method1() called");

        // доступ ко всем полям возможен
        publicField = 1000;

        // доступ к статическим полям возможен,
        // изнутри класса модификаторы доступа не ограничиват доступ
        publicStaticField1 = 1500;
        privateStaticField = 2000;

        // доступ к статическим методам возможен
        // изнутри класса модификаторы доступа не ограничиват доступ
        publicStaticMethod1();
        privateStaticMethod2();

        // доступ к нестатическому методу возожен
        method2();

        // доступ через this возможен как для обычных, так и дл статических членов
        // для статических переменных это довольно бессмысленно, хотя работает
        this.publicField = 1000;
        this.publicStaticField1 = 1500;
        this.privateStaticField = 2000;
        this.publicStaticMethod1();
        this.privateStaticMethod2();
        this.method2();

        // доступ через имя класса возможен для статических членов, как и извне
        AccessToStaticMembers.publicStaticField1 = 1500;
        AccessToStaticMembers.privateStaticField = 2000;
        AccessToStaticMembers.publicStaticMethod1();
        AccessToStaticMembers.privateStaticMethod2();
    }

    void method2() {
        System.out.println("Method2() called");
    }

    public static void publicStaticMethod1() {
        System.out.println("publicStaticMethod1() called");

        // доступ ко всем нестатическим полям невозможен
        // publicField = 1000;

        // доступ к статическим полям возможен,
        // изнутри класса модификаторы доступа не ограничиват доступ
        publicStaticField1 = 1500;
        privateStaticField = 2000;

        // доступ к статическим методам возможен
        // изнутри класса модификаторы доступа не ограничиват доступ
        privateStaticMethod2();

        // доступ к нестатическому методу невозожен
        // method2();

        // в статическом методе не существует this,
        // поэтому доступ к нестатическим членам класса невозможен

        // доступ через имя класса возможен для статических членов, как и извне
        AccessToStaticMembers.publicStaticField1 = 1500;
        AccessToStaticMembers.privateStaticField = 2000;
        AccessToStaticMembers.privateStaticMethod2();
    }

    private static void privateStaticMethod2() {
        System.out.println("privateStaticMethod2() called");
    }
}
