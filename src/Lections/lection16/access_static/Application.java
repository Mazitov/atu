package  Lections.lection16.access_static;

public class Application {
    public static void main (String args []) {
        // Иллюстрация доступа к статическим и нестатическим членам класса извне класса
        // доступ по имени класса: работает для статических членов класса,
        // модификаторы доступу принимаются во внимание
        AccessToStaticMembers.publicStaticMethod1();
        AccessToStaticMembers.publicStaticField1 = 100;

        // нет доступа, т. к. поля объявлены как private
        // AccessToStaticMembers.privateStaticField = 200;
        // AccessToStaticMembers.privateStaticMethod2();

        // для нестатических членов нет доступа по имени класса
        // AccessToStaticMembers.publicField = 300;
        // AccessToStaticMembers.method1();

        AccessToStaticMembers instance = new AccessToStaticMembers();
        // через ссылку на экземпляр можно получить доступ как к статическим,
        // так и к нестатическим публичным членам класса
        instance.publicField = 500;
        instance.method1();
        instance.publicStaticMethod1();

        // нет доступа, т. к. поля объявлены как private
        // instance.privateStaticField = 200;
        // instance.privateStaticMethod2();
    }
}
