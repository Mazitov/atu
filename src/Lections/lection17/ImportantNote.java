package Lections.lection17;

public class ImportantNote extends Note {
    private boolean important;

    public ImportantNote(String author, String text, boolean important) {
        super(author, text);
        this.important = important;
    }

    public boolean isImportant() {
        return important;
    }

    public void setImportant(boolean important) {
        this.important = important;
    }

    @Override
    public void clear() {
        super.clear();
        important = false;
        System.out.println("Important Note is cleared.");
    }
}
