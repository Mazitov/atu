package Lections.lection17;

public class Note {
    private String author;
    private String text;

    public Note(String author, String text) {
        this.author = author;
        this.text = text;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public void clear() {
        author = null;
        text = null;
        System.out.println("Note is cleared.");
    }
}
