package lec15;

public class CopyCat {

    private String nameStr;
    private double ageYears;
    private double weightKg;

    public static void main(String args[]) {
        CopyCat cat1 = new CopyCat("Barsik", 1.2, 4.5);
        CopyCat cat2 = new CopyCat(cat1);
        CopyCat cat3 = new CopyCat(cat2);
        System.out.println(cat1);
        System.out.println(cat2);
        System.out.println(cat3);
    }

    // объявим конструктор c параметрами
    CopyCat(String name, double age, double weight) {
        nameStr = name;
        ageYears = age;
        weightKg = weight;
    }

    CopyCat(CopyCat otherCat) {
        nameStr = "Other " + otherCat.nameStr;
        ageYears = otherCat.ageYears;
        weightKg = otherCat.weightKg;
    }

    @Override
    public String toString() {
        return "This is a cat!\nName is: " + nameStr + "\nWeight is: " + weightKg +  "\nAge is: " + ageYears + "\n";
    }
}
