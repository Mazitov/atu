package lec15;

public class InitBlocks {
    private String nameStr;
    private double ageYears;
    private double weightKg;
    private boolean foodNeeded;

    public static void main(String args[]) {
        InitBlocks cat1 = new InitBlocks("Barsik", 1.2, 4.5);
    }

    // это - блок инициализации, он выполняется до конструктора
    // он похож на конструктор, только не принимает параметры
    {
        System.out.println("In init block 1");
        foodNeeded = true; // делаем, что нужно
    }

    // объявим конструктор c параметрами
    InitBlocks(String name, double age, double weight) {
        System.out.println("In Constructor");
        nameStr = name;
        ageYears = age;
        weightKg = weight;
    }

    // блоков инициализации может быть несколько, они выполняются в порядке определения
    {
        System.out.println("In init block 2");
    }
}
