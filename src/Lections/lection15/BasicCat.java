package lec15;

public class BasicCat {
    // Мы могли бы захардкодить значения. Но как быть с экземплярами классов?
    /*
    String nameStr = "Barsik";
    double ageYears = 1.8;
    double weightKg = 4.6;
    */

    private String nameStr;
    private double ageYears;
    private double weightKg;

    public static void main(String args[]) {
        BasicCat cat1 = new BasicCat("Barsik", 1.2, 4.5);
        BasicCat cat2 = new BasicCat("Ryzhik", 0.3, 1.1);
        BasicCat cat3 = new BasicCat("Matilda");
    }

    // объявим конструктор c параметрами
    BasicCat(String name, double age, double weight) {
        nameStr = name;
        ageYears = age;
        weightKg = weight;
    }

    // Другой конструктор - на вход принимает только имя, например, других данных нет
    BasicCat(String name) {
        nameStr = name;
        ageYears = 0;
        weightKg = 0;
    }
}
