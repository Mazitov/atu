package Lections.lection15.finalizeclass;

public class Application {
    public static void main(String args[]) throws InterruptedException {
        {
            System.out.println("Before creating class");
            Finalize withFinalize = new Finalize();
            System.out.println("After creating class ");
        }
        System.out.println("Class went out of scope...");
        Integer dummy[] = new Integer[100_000]; // займем немного памяти, чтобы была нужда в сборке мусора
        System.out.println("Before gc call");
        System.gc(); // скажем сборщику мусора, что мы хотели бы прибраться. Вызов его не гарантирован
        // это все равно, что повесить на дверь в отеле табличку о необходимости уборки
        System.out.println("After gc call");
        Thread.sleep(1000);
    }
}
