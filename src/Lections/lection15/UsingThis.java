package lec15;

public class UsingThis {
    String name;
    String surname;
    boolean isFemale;

    public static void main(String args[]) {
        UsingThis person1 = new UsingThis("Ivar", "Olafsson", false);
        UsingThis person2 = new UsingThis("Joi", "Ngyen", true);

        // вызов методов по цепочке - chaining
        person2.setName("Brunhild").setSurname("Ragnarsdotter");

        person1.getMarryed(person2);
    }

    // сокрытие имен переменных не позволяет инициализировать поля класса напрямую,
    // однако, это можно сделать через this
    UsingThis(String name, String surname, boolean isFemale) {
        this.name = name;
        this.surname = surname;
        this.isFemale = isFemale;
    }

    // это называется "chaining", посмотрите на использование в main()
    UsingThis setName(String name) {
        this.name = name;
        return this;
    }

    UsingThis setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    // передача this
    public void getMarryed(UsingThis that) {
        if (isFemale == that.isFemale) {
            // по Семейному Кодексу однополые браки у нас не разрешены
            return;
        }

        if (isFemale) {
            takeSurname(that);
        } else {
            that.takeSurname(this); // передача ссылки на this
        }
    }

    // взять фамилию
    private void takeSurname(UsingThis that) {
        surname = that.surname;
    }
}
