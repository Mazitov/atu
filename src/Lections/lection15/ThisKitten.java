package lec15;

public class ThisKitten {
    private String nameStr;
    private double ageYears;
    private double weightKg;

    public static void main(String args[]) {
        ThisKitten cat1 = new ThisKitten("Barsik", 1.2, 4.5);
        ThisKitten cat2 = new ThisKitten("Ryzhik", 0.3, 1.1);
        cat1.isKitten();
        cat2.isKitten();
    }

    ThisKitten(String name, double age, double weight) {
        nameStr = name;
        ageYears = age;
        weightKg = weight;
    }

    boolean isKitten() {
        System.out.print("Кот " + nameStr + " это ");
        if (ageYears < 1) { // this.ageYears
            System.out.println("Котенок");
            return true;
        }
        else {
            System.out.println("Взрослый");
            return false;
        }
    }

}
