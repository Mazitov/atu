package lec15;

public class DefaultConstructor {
    public static void main(String args[]) {
        // у этого класса остутсвует явно объявленный конструктор,
        // но присутствует конструктор по умолчанию
        DefaultConstructor withDefaultConstructor = new DefaultConstructor();
    }
}
