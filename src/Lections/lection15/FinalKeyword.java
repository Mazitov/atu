package lec15;

public class FinalKeyword {
    private final String[] paws = new String[4];

    private final String nameStr;
    private double ageYears;
    private double weightKg;

    public static void main(String args[]) {
        FinalKeyword cat3paws = new FinalKeyword("Barsik", 1.2, 4.5);
        String[] pawsArray = {"paw1", "paw2", "paw3", "paw4"};
        cat3paws.addPaws(pawsArray);
        cat3paws.printPaws();
    }

    FinalKeyword(String name, double age, double weight) {
        nameStr = name;
        ageYears = age;
        weightKg = weight;
    }

    void addPaws(String[] paws) {
        // тут мы успешно присваиваем значения по неизменяемой ссылке на массив
        System.arraycopy(paws,0, this.paws,0 , this.paws.length);
    }

    void printPaws() {
        int i = 0;
        for(String paw : paws) {
            System.out.println("Paw " + ++i + " is: " + paw);
        }
    }

    /*
    void changeName(String name) {
        nameStr = name; // не скомпилируется, так как поле объявлено как final
    }
    */

    // квалификатор final здесь гарантирует, что никто не поменяет переменную newAge
    // обычно менять параметр не рекомендуется, но если метод слишком длинный и сигнатура не видна
    // на экране, кто-то может забыться и поменять его значение
    void changeAge(final double newAge) {
        // newAge = 2000; // не скомпилируется из-за final
        // пример final
        final int monthsInYear = 12;
        // monthsInYear = 1; не скомпилируется из-за final
        ageYears = newAge;
        double ageMonths = ageYears * monthsInYear;
    }

    void changeWeight(double newWeight) {
        weightKg = newWeight;
    }
}
