package lec15;

class ChainedThis {
    private String name;
    private double age;
    private double weight;

    public static void main(String args[]) {
        ChainedThis chainedCat1 = new ChainedThis("Dymok");
        ChainedThis chainedCat2 = new ChainedThis("Bandit", 2d);
        ChainedThis chainedCat3 = new ChainedThis("Murka", 3.4, 6d);
    }

    ChainedThis(String name) {
        this.name = name; // этот конструктор устанавливает имя котика...
    }

    ChainedThis(String name, double age) {
        this(name); // Вызов конструктора по цепочке
        this.age = age;
    }

    ChainedThis(String name, double age, double weight) {
        this(name, age);
        this.weight = weight;
    }
}

