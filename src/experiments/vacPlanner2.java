package experiments;

import java.util.Scanner;

public class vacPlanner2 {


        public static void main(String[] args) {
            Extra();
            Greeting();
            TravelandBudget();
            TimeDifference();
            CountryArea();
        }

        public static void Greeting() {
            Scanner input = new Scanner(System.in);
            System.out.print("Welcome to Vacation Planner!\nWhat is your name? ");
            String name = input.nextLine();
            System.out.print("Nice to meet you " + name + ", where are you travelling to? ");
            String Des = input.nextLine();
            System.out.println("Great! " + Des + " sounds like a fun trip!");
            System.out.println("***********");
            System.out.println("");
        }

        public static void TravelandBudget() {
            Scanner input = new Scanner(System.in);
            System.out.print("How many days are you going to spend travelling? ");
            int Days = input.nextInt();
            System.out.print("How much money, in USD are you planning to spend on your trip? ");
            Double SpendMoney = input.nextDouble();
            System.out.print("What is the three letter currency symbol for your travel destination? ");
            String Currency = input.next();
            System.out.print("How many " + Currency + " are there in 1 USD? ");
            double Conversion = input.nextDouble();
            System.out.println("");
            int Hours = Days * 24;
            int Minutes = Hours * 60;
            double PerDay = SpendMoney / Days;
            double Forn = SpendMoney * Conversion;
            double FornPerDay = Forn / Days;
            System.out.println("If you are travelling for " + Days + " days that is the same as " + Hours + " hours or " + Minutes + " minutes");
            System.out.println("If you are going to spend $" + SpendMoney + " USD that means per day you can spend up to " + Truncate2Decimal(PerDay) + " USD");
            System.out.println("Your total budget in " + Currency + " is " + Forn + " " + Currency + ", which per day is " + Truncate2Decimal(FornPerDay) + " " + Currency);
            System.out.println("***********");
            System.out.println("");
        }

        public static void TimeDifference() {
            Scanner input = new Scanner(System.in);
            System.out.print("What is the time difference, in hours, between your home and your destination? ");
            int TimeDiff = input.nextInt();
            System.out.println("That means that when it is midnight at home it will be " + (24 + TimeDiff) % 24 + ":00 in your travel destination");
            System.out.println("and when it is noon at home it will be " + (12 + TimeDiff) % 24 + ":00");
            System.out.println("***********");
            System.out.println("");
        }

        public static void CountryArea() {
            Scanner input = new Scanner(System.in);
            System.out.print("What is the square area of your destination country in km2? ");
            int KM2 = input.nextInt();
            double Miles2 = KM2 * 0.38610;
            System.out.println("In miles2 that is " + Truncate2Decimal(Miles2));
            System.out.println("***********");
            System.out.println("");
        }

        public static void Extra() {
            /* To Change Decimal Place.*/
        }

        public static Double Truncate2Decimal(Double num) {
            return (int) (num * 100) / 100.0;
        }
    }

