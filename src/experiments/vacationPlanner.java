package experiments;

import java.util.Scanner;

public class vacationPlanner {
    public static void main(String[] args) {


        System.out.println("Welcome to Vacation Planner! \n What is your name?");
        Scanner name = new Scanner(System.in);
        System.out.println("Nice to meet you " + name.nextLine() + ", where are you travelling to?");
        Scanner city = new Scanner(System.in);
        String city1 = city.nextLine();
        System.out.println("Great! " + city.nextLine() + " sounds like a great choice.");


        System.out.println("How many days are you going to spend?");
        Scanner days = new Scanner(System.in);
        int days1 = days.nextInt();
        int hours = days.nextInt() * 24;
        int min = hours * 60;

        System.out.println("How much money, in USD, are you planning to spend on your trip?");
        Scanner budget = new Scanner(System.in);
        double budget1 = budget.nextDouble();

        System.out.println("What is the currency symbol for their destination?");
        Scanner currency = new Scanner(System.in);
        String currency1 = currency.next();

        System.out.println("How much " + currency.next() + " in 1 USD?");
        Scanner exchangeRate = new Scanner(System.in);
        double exchangeRate1 = exchangeRate.nextDouble();



        System.out.println("If you are travelling for " + days1 + " days to " + city1 + " that is the same as " + hours + " hours or " + min + " minutes. ");
        System.out.println("If you are going to spend $" + budget1 + " USD that means per day you can spend up to $" + (budget1 / days1) + " USD.");
        System.out.println("Your total budget in " + currency1 + " is " + (budget1 * exchangeRate1) + " " + currency1 + ", which per day is " + ((budget1 * exchangeRate1) / days1) + " " + currency1);


    }
}
