package experiments;
import  java.util.Arrays;
public class Class1 {
    public void printing() {
        System.out.println("I am Class1");
    }
}


class Class2 extends Class1 {

   public void printing() {
        System.out.println("I am Class2");
    }
}


class Class3 extends Class1 {
    public void printing() {
        System.out.println("I am Class3");
    }
}

class Class4 extends Class2 {
    public void printing() {
        System.out.println("I am Class4");
    }
}

class Class5 extends Class3 {
    public void printing() {
        System.out.println("I am Class5");
    }
}

class Testings {
    public static void main(String[] args) {
      /*  Class1 obj1 = new Class1();
        obj1.printing();
        Class2 obj2 = new Class4();
        obj1 = (Class2) obj2;
        obj1.printing();
        obj2 = (Class2) obj2;
        obj2.printing();
        obj2 = (Class4) obj2;
        obj2.printing();*/

        Class1 obj11 = new Class1();
        Class2 obj12 = new Class2();
        obj11 = (Class2) obj12;
        System.out.println(obj11.equals(obj12));



        int[] array = {1, 5, 4, 3, 7}; //объявляем и инициализируем массив
        System.out.println(array);//пытаемся вывести наш массив на экран без метода toString - получаем 16-ричное число
        System.out.println(Arrays.toString(array));//печатаем массив "правильно"
        Arrays.sort(array, 0, 4); //сортируем весь массив от нулевого до четвёртого члена
        System.out.println(Arrays.toString(array));//выводим отсортированный массив на экран
        int key = Arrays.binarySearch(array, 5); // ищем key - число 5 в отсортированном массиве.
        //метод binarySearch выдаст индекс элемента остортированного массива, в котором "спрятано" искомое число
        System.out.println(key);//распечатываем индекс искомого числа
        System.out.println(Arrays.binarySearch(array, 0));//а теперь попробуем найти число, которого в массиве нет,
        // и сразу же выведем результат на экран




    }

}
