package exam;

public class Auto extends Transport {
    private boolean withConditioner;

    public Auto(int pricePerMinute, String modelName, boolean withConditioner) {
        super(pricePerMinute, modelName);
        this.withConditioner = withConditioner;
    }

    public boolean isWithConditioner() {
        return withConditioner;
    }

    public void setWithConditioner(boolean withConditioner) {
        this.withConditioner = withConditioner;
    }

    @Override
    public String toString() {
        return "Модель автомобиля: " + super.getModelName() +
                (withConditioner ? ", c кондиционером" : ", без кондиционера") +
                ", цена: $" + super.getPricePerMinute() + " за минуту.";
    }
}
