package exam;

public class Motorbike extends Transport {
    private boolean withKolyaska;

    public Motorbike(int pricePerMinute, String modelName, boolean withKolyaska) {
        super(pricePerMinute, modelName);
        this.withKolyaska = withKolyaska;
    }

    public boolean isWithKolyaska() {
        return withKolyaska;
    }

    public void setWithKolyaska(boolean withKolyaska) {
        this.withKolyaska = withKolyaska;
    }

    @Override
    public String toString() {
        return "Модель мотоцикла: " + super.getModelName() +
                (withKolyaska ? ", c коляской" : ", без коляски") +
                ", цена: $" + super.getPricePerMinute() + " за минуту.";
    }
}
