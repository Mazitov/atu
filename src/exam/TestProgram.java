package exam;

import java.util.ArrayList;

public class TestProgram {
    public static void main(String[] args) {
        Auto autoKia = new Auto(30, "Kia", false);
        Auto autoBMW = new Auto(60, "BMW", true);
        Motorbike bikeMercedes = new Motorbike(80, "Mercedes", true);
        Motorbike bikeBMW = new Motorbike(40, "BMW", false);
        Motorbike bikeSuzuki = new Motorbike(30, "Suzuki", false);

        new CarsharingCompany(new Transport[]{autoKia, autoBMW, bikeBMW, bikeMercedes, bikeSuzuki});

        Customer Alex = new Customer("AB", "Александр");
        Customer Dmitry = new Customer("A", "Дмитрий");
        Customer Anton = new Customer("C", "Антон");

        CarsharingCompany.getTransportList();
        System.out.println();

        CarsharingCompany.getAvailableTransport(Alex);
        System.out.println();
        CarsharingCompany.getAvailableTransport(Dmitry);
        System.out.println();
        CarsharingCompany.getAvailableTransport(Anton);


    }
}
