package exam;

public class Transport {
    private int pricePerMinute;
    private final String modelName;

    public Transport(int pricePerMinute, String modelName) {
        this.pricePerMinute = pricePerMinute;
        this.modelName = modelName;
    }

    public int getPricePerMinute() {
        return pricePerMinute;
    }

    public void setPricePerMinute(int pricePerMinute) {
        this.pricePerMinute = pricePerMinute;
    }

    public String getModelName() {
        return modelName;
    }
}
