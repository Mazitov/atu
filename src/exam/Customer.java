package exam;

public class Customer {
    private String drivingLicience;
    private final String customerName;

    public Customer(String drivingLicience, String customerName) {
        this.drivingLicience = drivingLicience;
        this.customerName = customerName;
    }

    public String getDrivingLicience() {
        return drivingLicience;
    }

    public void setDrivingLicience(String drivingLicience) {
        this.drivingLicience = drivingLicience;
    }

    public String getCustomerName() {
        return customerName;
    }
}
