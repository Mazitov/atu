package exam;

import java.util.ArrayList;

public class CarsharingCompany {
    public static ArrayList<Transport> transportList = new ArrayList<Transport>();

    public CarsharingCompany(Transport transport) {
        transportList.add(transport);
    }

    public CarsharingCompany(Transport[] transport) {
        for (Transport i : transport) {
            transportList.add(i);
        }
    }


    public static void getTransportList() {
        System.out.println("Весь автопарк: ");
        for (Transport i : transportList) {
            System.out.println(i.toString());
        }
    }

    public static void getAvailableTransport(Customer customer) {
        if (customer.getDrivingLicience().equals("A")) {
            System.out.println(customer.getCustomerName() + "! Для ваших категорий прав (A) доступны следующие модели мотоциклов: ");
            for (Transport i : transportList) {
                if (i instanceof Motorbike) {
                    System.out.println(i.toString());
                }
            }

        } else if (customer.getDrivingLicience().equals("AB")) {
            System.out.println(customer.getCustomerName() + "! Для ваших категорий прав (AB) доступны следующие модели автомобилей и мотоциклов: ");
            for (Transport i : transportList) {
                System.out.println(i.toString());
            }
        } else {
            System.out.println(customer.getCustomerName() + "! Для вас нет доступных траспортных средств.");
        }

    }


}
