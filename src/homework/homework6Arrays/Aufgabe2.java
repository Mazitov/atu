package homework.homework6Arrays;

public class Aufgabe2 {
    public static void main(String[] args) {
        // Объявить и проинициализировать массив, содержащий положительные и
        // отрицательные целые числа, вычислить сумму только четных положительных элементов.

        int[] massiv = {11, 0, 48, 487, -7479, 18, 47, -8, 7449, 4, 1, 1234, 74, 494, -41};
        int sum = 0;

        for (int elem : massiv) {
            if ((elem % 2) == 0 && (elem > 0)) sum += elem;
        }
        System.out.print("Сумма положительных и чётных элементов массива равна " + sum);

    }
}
