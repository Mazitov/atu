package homework.homework6Arrays;

public class Aufgabe1 {
    public static void main(String[] args) {
        // Выполнить реверсирование массива (первый элемент становится последним, последний - первым и т.д).
        // Вывести элементы первоначального массива и после выполнения реверсирования.

        double[] massiv = {11, 49, 48, 487, 7479.1, 18, 47, 8, 7449.4, 4, 1, 1234.474, .449, 4947, 41};
        int a = 0;
        double b;
        double[] massiv2 = new double[massiv.length];
        System.out.print("Массив до реверсирования: {");
        for (double elem : massiv) {
            System.out.print(elem + ", ");
        }

        System.out.println("}");
        System.out.print("Массив после реверсирования: {");

        for (int i = (massiv.length - 1); i >= 0; --i) {
            massiv2[a] = massiv[i];
            ++a;
            //System.out.println(" massiv[a]");
        }
        massiv = massiv2;

        for (double elem : massiv) {
            System.out.print(elem + ", ");
        }
        System.out.println("}");
    }
}
