package homework.homework5ConditionalOperators;

public class Aufgabe6 {
    public static void main(String[] args) {
        //Найти количество цифр заданного числа n типа long (например, 1000 – 4 цифры)
        int chislo, kolvo;
        chislo = 87654321;  // указываем число
        kolvo = 1;  //  задаём минимальное количество цифр в числе
        System.out.print("Количесвто цифр в числе " + chislo);
        if (chislo < 0) chislo = -chislo;
        for (; chislo >= 1; kolvo++) {
            chislo /= 10;
            if (chislo < 1) break;
        }
        System.out.print(" равно " + kolvo);
    }
}
