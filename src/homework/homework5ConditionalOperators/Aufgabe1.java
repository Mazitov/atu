package homework.homework5ConditionalOperators;

public class Aufgabe1 {
    public static void main(String[] args) {
        // По номеру времени года вывести его название.
        // Например, для 0 – вывести «зима», для 3 – «лето»

        int vremyaGoda;
        vremyaGoda = 3;

        switch (vremyaGoda) {
            case (0):
                System.out.println("Зима");
                break;
            case (1):
                System.out.println("Весна");
                break;
            case (2):
                System.out.println("Лето");
                break;
            case (3):
                System.out.println("Осень");
                break;
        }
    }
}