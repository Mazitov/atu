package homework.homework5ConditionalOperators;

public class Aufgabe8 {
    public static void main(String[] args) {
        //Используя генератор случайных чисел (long rand = Math.round(Math.random() * макс_число)), который генерирует целые числа от 0 до макс_число,
        // написать генератор супергеройских имен.
        //Для этого сгенерировать два числа, и, пользуюсь таблицей, вывести получившееся имя с эпитетом. Примерная таблица:
        //
        //Эпитет:
        //0 - РАДИОАКТИВНЫЙ, 1 - ГРЕЧНЕВЫЙ, 2 - ДЕМОНИЧЕСКИЙ,  3 - ПРИЗРАЧНЫЙ, 4 - ОЗОРНОЙ 5 - ПРИЗРАЧНЫЙ 6 - КОСМИЧЕСКИЙ 7 - ЗВЕЗДНЫЙ 8 - СЕКСУАЛЬНЫЙ 9 - НЕПОБЕДИМЫЙ
        //Имя:
        //0 - КАПИТАН, 1 - ЭЛЬФ, 2 - ПИНГВИН, 3 - ИНДЕЕЦ, 4 - ГНОМ, 5 - УПЫРЬ, 6 - БОРОДАЧ, 7 - БОБЕР, 8 - КОРОЛЬ, 9 - ТОЛСТОПУЗ, 10 - КИЛЛЕР
        //
        //Например, выпало 6 и 9, имя - Космический Толстопуз. Можно использовать свои таблицы.

        long adj00 = Math.round(Math.random() * 9);
        long name00 = Math.round(Math.random() * 9);
        //String adj, name;  - создать переменную стрингу и присваивать ей значение в каждом кейсе не удалось. в итоге возвращаясь из кейса переменная оставалась не проинициализированной
        // поэтому в каждый кейс добавлено System.out.print()


        //System.out.println(name00);
       // System.out.println(adj00);

        switch ((int) adj00) {
            case 0:
                System.out.print("РАДИОАКТИВНЫЙ");
                break;
            case 1:
                System.out.print("ГРЕЧНЕВЫЙ");
                break;
            case 2:
                System.out.print("ДЕМОНИЧЕСКИЙ");
                break;
            case 3:
                System.out.print("ПРИЗРАЧНЫЙ");
                break;
            case 4:
                System.out.print("ОЗОРНОЙ");
                break;
            case 5:
                System.out.print("ПРИЗРАЧНЫЙ");
                break;
            case 6:
                System.out.print("КОСМИЧЕСКИЙ");
                break;
            case 7:
                System.out.print("ЗВЕЗДНЫЙ");
                break;
            case 8:
                System.out.print("СЕКСУАЛЬНЫЙ");
                break;
            case 9:
                System.out.print("НЕПОБЕДИМЫЙ");
        }
        System.out.print(" ");
        switch ((int) name00) {
            case 0:
                System.out.print("КАПИТАН");
            case 1:
                System.out.print("ЭЛЬФ");
                break;
            case 2:
                System.out.print("ИНДЕЕЦ");
                break;
            case 3:
                System.out.print("ГНОМ");
                break;
            case 4:
                System.out.print("УПЫРЬ");
                break;
            case 5:
                System.out.print("БОРОДАЧ");
                break;
            case 6:
                System.out.print("БОБЕР");
                break;
            case 7:
                System.out.print("КОРОЛЬ");
                break;
            case 8:
                System.out.print("ТОЛСТОПУЗ");
                break;
            case 9:
                System.out.print("КИЛЛЕР");
        }

    }
}
