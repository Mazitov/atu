package homework.homework5ConditionalOperators;

public class Aufgabe7 {
    public static void main(String[] args) {
        // Проверить, является ли число степенью двойки. Вывести «да» или «нет». По желанию вывести степень.

        final int chislo = 2048;
        int a = chislo;
        int i = 0;  // переменная для поискать степени двойки
        for (; (a > 0); i++) {

            if (a == 1)
                break;                  // возможно, можно избежать этих трх условий и заменить на что-то более красивое. но я не смог придумать.
            if ((a % 2) != 0) {
                i = 0;

            }
            a /= 2;

        }

        if ((i == 0) && (a != 1)) {
            System.out.print("НЕТ! " + chislo + " не является степенью числа 2");

        } else {
            System.out.print("ДА! " + chislo + " является 2 в степени " + i);
        }

    }
}
