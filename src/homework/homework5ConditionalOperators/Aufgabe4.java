package homework.homework5ConditionalOperators;

public class Aufgabe4 {

    public static void main(String[] args) {
        //Перевернуть цифры числа. Дано целое число, например 1234567. Вывести 7654321

        int chislo = 9654120;  //
        System.out.print("Если перевернуть цифры числа " + chislo + ", то получим ");
        for (;;) {
            int i = chislo % 10;
            System.out.print(i);
            chislo /= 10;
            if (chislo < 1 && chislo >= 0) break;
        }
        // но к сожалению этот метод не работает если число написано вот так - 0001124
    }
}
