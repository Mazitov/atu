package homework.homework5ConditionalOperators;

public class Aufgabe2 {
    public static void main(String[] args) {
        //Даны 3 числа, a, b, c. Проверить истинность a < b < c. Вывести результат.

        int a, b, c;
        a = 45;
        b = 120;
        c = 1485;

        System.out.print("Выражение a < b < c - ");

        if ((a < b && b < c))  {
            System.out.print("верно.");
        } else {
            System.out.print("ложь.");
        }

    }
}
