package homework.homework5ConditionalOperators;

public class Aufgabe3 {
    public static void main(String[] args) {
        // Найти факториал числа (произведение всех чисел, от 1 и до числа.
        // Например, факториал  5 = 1 * 2 * 3 * 4 * 5)

        int chi = 3; // Дано число
        int fkt = 1;  // Переменная для факториала

        System.out.print("Факториал числа " + chi);

        for (; chi >= 1; --chi) {             // Создаём цикл, который умножает число на переменную fkt, а после уменьшае на 1
            fkt *= chi;
        }

        System.out.print(" равен " + fkt);

    }
}
