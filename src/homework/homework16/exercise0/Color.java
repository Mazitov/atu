package homework.homework16.exercise0;

/*Создайте класс, хранящий доступные всем целочисленные
 константы: RED - 0xFF000000, GREEN - 0x00FF0000, BLUE - 0x0000FF00*/

public class Color {

    public static final int red = 0xFF000000;
    public static final int green = 0x00FF0000;
    public static final int blue = 0x0000FF00;

}
