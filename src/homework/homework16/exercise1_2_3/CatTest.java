package homework.homework16.exercise1_2_3;

import java.util.ArrayList;
import java.util.Arrays;

public class CatTest {
    public static void main(String[] args) {
        BasicCat cat1 = new BasicCat("Barsik", 1.2, 4.5);
        BasicCat cat2 = new BasicCat("Ryzhik", 0.3, 1.1);

        BasicCat cat3 = new BasicCat("Ryzhik", 0.3, 0.1);
        BasicCat cat4 = new BasicCat("Ryzhik", 0.3, 0.9);
        BasicCat cat5 = new BasicCat("Barsik", 1.2, 4.5);
        BasicCat cat6 = new BasicCat("Ryzhik", 0.3, 1.1);

        BasicCat cat7 = new BasicCat("Ryzhik", 0.3, 0.1);
        BasicCat cat8 = new BasicCat("Ryzhik", 0.3, 0.9);
        BasicCat cat9 = new BasicCat("Barsik", 1.2, 4.5);
        BasicCat cat10 = new BasicCat("Ryzhik", 0.3, 1.1);

        BasicCat cat11 = new BasicCat("Ryzhik", 0.3, 0.1);
        BasicCat cat12 = new BasicCat("Ryzhik", 0.3, 0.9);

        System.out.println(BasicCat.getAge(cat1));
        System.out.println(BasicCat.getAge(cat2));
        System.out.println(BasicCat.getAge(cat3));

        BasicCat.getNumKittens();

        System.out.println(BasicCat.getNumberOfObjectArrays());

        ArrayList<BasicCat> arrayListCats = new ArrayList<BasicCat>(Arrays.asList(BasicCat.getCatArray()));
        System.out.println(arrayListCats);
    }
}
