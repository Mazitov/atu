package homework.homework16.exercise1_2_3;

/*1) В классе BasicCat из прошлого задания (с полями name, age, weight)
 определить метод, принимающий на вход экземпляр класса Cat и печатающий
 возраст кошки, приведенный к условному “человеческому”. Для получения
 этого значения нужно умножить возраст кошки на 7. Хранить константу 7 в
  классе. Вызвать этот метод.*/

/*2) Определите в классе BasicCat статическую переменную numKittens,
 и добавьте логику в конструктор, чтобы эта переменная увеличивалась,
 когда создается экземпляр класса с возрастом, меньшим 1 года.
 Добавьте метод, печатающий количество созданных котят за всю
 историю существования класса.*/

/*3) Добавьте массив в класс BasicCat, который бы хранил ссылки на все когда-либо
созданные экземпляры в статической переменной. Хранить переменные в массиве,
 корректно обрабатывать ситуацию переполнения массива создавать новый массив,
 не терять то, что было. Добавьте метод, который выводит на экран количество
 созданных экземпляров.*/

public class BasicCat {
    private String nameStr;
    private double ageYears;
    private double weightKg;
    private static final double YEAR_MULTIPLIER = 7.0;
    private static int numKittens = 0;
    private static int numberOfObjectArrays = 0;
    private static final int ARRAY_BLOCK_SIZE = 5;

    private static BasicCat[] catArray = new BasicCat[ARRAY_BLOCK_SIZE];


    BasicCat(String name, double age, double weight) {
        nameStr = name;
        ageYears = age;
        weightKg = weight;
        if (ageYears < 1) ++numKittens;
        createObjectArray(this);
    }

    public static double getAge(BasicCat cat) {
        return cat.ageYears * YEAR_MULTIPLIER;
    }

    public static void getNumKittens() {
        System.out.println("Number of Kittens is " + numKittens + ".");
    }

    private static void createObjectArray(BasicCat cat) {
        if (numberOfObjectArrays < catArray.length) {
            catArray[numberOfObjectArrays] = cat;
            ++numberOfObjectArrays;


        } else {
            BasicCat[] catArray2 = new BasicCat[catArray.length + ARRAY_BLOCK_SIZE];
            for (int y = 0; y < catArray.length; ++y) {
                catArray2[y] = catArray[y];
            }
            catArray = catArray2;
            catArray[numberOfObjectArrays] = cat;
            ++numberOfObjectArrays;

        }

    }

    public static int getNumberOfObjectArrays() {
        return numberOfObjectArrays;
    }

    public static BasicCat[] getCatArray() {
        return catArray;
    }

    @Override
    public String toString() {
        return "{name '" + nameStr + ", " + ageYears + " years, " + weightKg + "kg}";
    }
}
