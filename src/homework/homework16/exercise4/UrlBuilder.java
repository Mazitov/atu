package homework.homework16.exercise4;
/*Создайте класс UrlBuilder и класс Url. В классе Url определите строковые поля schema, hostname, resource.
 Пусть поле schema будет иметь значение “http”, а resource - пустая строка. Создайте сеттеры и геттеры для
полей schema и resource. В классе UrlBuilder создайте конструктор, который принимает hostname и создает
сущность Url, методы setScheme и setResource, которые вызывают соответствующие сеттеры у созданного Url.
Создайте также метод build(), который возвращает строку, созданную по информации, содержащейся в классе
Url по шаблону:

 <schema>://<hostname>/<resource>. Примеры использования:

String url = new UrlBuilder(google.com).setSchema(“https”).setResource(“users”).build(); // url при этом содержит https://google.com/users
String url = new UrlBuilder(yandex.ru).build(); // url при этом содержит http://yandex.ru
String url = new UrlBuilder(t-systems.com).setSchema(“https”).setResource(“contacts”).build(); // url при этом содержит https://t-systems.com/contacts*/


public class UrlBuilder {
    private String url;
    private Url urlObject;
    private static int i = 0;

    public UrlBuilder(String hostname) {
        urlObject = new Url(hostname);
    }

    public String build() {
        if (urlObject.getResource().equals("")) {
            return urlObject.getSchema() + "://" + urlObject.getHostname();
        } else {
            return urlObject.getSchema() + "://" + urlObject.getHostname() + "/" + urlObject.getResource();
        }
    }

    public UrlBuilder setResource(String resource) {
        urlObject.setResource(resource);
        return this;
    }

    public UrlBuilder setSchema(String schema) {
        urlObject.setSchema(schema);
        return this;
    }
}
