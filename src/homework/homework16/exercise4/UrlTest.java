package homework.homework16.exercise4;

public class UrlTest {
    public static void main(String[] args) {
        String url = new UrlBuilder("google.com").setSchema("https").setResource("sdf").build();
        String url2 = new UrlBuilder("yandex.ru").build();
        String url3 = new UrlBuilder("t-systems.com").setSchema("https").setResource("contacts").build();
        System.out.println(url);
        System.out.println(url2);
        System.out.println(url3);
    }
}
