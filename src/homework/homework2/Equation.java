package homework.homework2;

public class Equation {
    public static void main(String[] args) {
        // Часть 1
        short aaaa = 1;
        short bbbb = -4;
        double cccc;
        cccc = (aaaa + bbbb) * (aaaa + bbbb);
        double dddd;
        dddd = aaaa * aaaa + 2 * aaaa * bbbb + bbbb * bbbb;
        System.out.print("1. C = " + cccc);
        System.out.println("; D = " + dddd);

        int a = 6;
        int b = -155;
        float c, d;

        c = (a - b) * (a - b);
        d = a * a - 2 * a * b + b * b;
        System.out.print("2. C = " + c);
        System.out.println("; D = " + d);

        long a2 = 634;
        long b2 = 105;
        float c2, d2;

        c2 = a2 * a2 - b2 * b2;
        d2 = (a2 - b2) * (a2 + b2);
        System.out.print("3. C = " + c2);
        System.out.println("; D = " + d2);

        // Часть 2
        float x = 5;
        float y = -9;
        float z, z1, z2, z3, w, w1, w2, w3;
        z = (x + y) * (x + y) * (x + y);
        w = (x * x * x) + 3 * x * x * y + 3 * x * y * y + (y * y * y);
        System.out.print("4. LEFT_1 = " + z);
        System.out.println("; RIGHT_1= " + w);

        z1 = (x - y) * (x - y) * (x - y);
        w1 = (x * x * x) - 3 * x * x * y + 3 * x * y * y - (y * y * y);
        System.out.print("5. LEFT_2 = " + z1);
        System.out.println("; RIGHT_2= " + w1);

        z2 = (x * x * x) - (y * y * y);
        w2 = (x - y) * (x * x + x * y + y * y);
        System.out.print("6. LEFT_3 = " + z2);
        System.out.println("; RIGHT_3= " + w2);

        z3 = (x * x * x) + (y * y * y);
        w3 = (x + y) * (x * x - x * y + y * y);
        System.out.print("7. LEFT_4 = " + z3);
        System.out.println("; RIGHT_4= " + w3);

        // Часть 3
        char a3 = 600;
        char b3 = 102;
        int c3, d3;

        //получается Char можно использовать в числовых расчётах, с учётом того, что Char принимает только положительные значения от 0 до 65536.
        //Но результатом расчётов должен быть другой числовой тип. Иначе расчёт не будет произведён.

        c3 = (a3 - b3) * (a3 - b3);
        d3 = a3 * a3 - 2 * a3 * b3 + b3 * b3;
        System.out.print("8. C (char) = " + c3);
        System.out.println("; D (char) = " + d3);

        //Часть 4
        System.out.println("\n \tLorem ipsum!\n" +
                "Dolor sit amet, consectetur adipiscing elit. \n" +
                "Maecenas quis sollicitudin augue.\n" +
                "\t\tNullam blandit!\n");


    }

}

