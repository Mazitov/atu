package homework.homework8Recursion;

public class fibonachiCycle {
    /*
    Необходимо вывести число фибоначчи двумя спобами, рекурсивно и в цикле.
    По желанию вывести время затраченно на каждый способ.
    Испльзуйте long currentTime = System.nanoTime();  для узнавания текущего времени в наносекундах
   */
    public static int a = 0; //

    public static int b = 0; //
    public static int c = 0; //
    public static int kolvo = 10; // количество цифр фибоначи

    public static void main(String[] args) {
        long currentTime = System.nanoTime();
        fibonachiCycle fibonachi = new fibonachiCycle();
        fibonachi.run();
        long currentTime2 = System.nanoTime();
        printTime((currentTime2-currentTime)/1000000);
    }

    private static void printTime(long time) {
        System.out.println("\nСalculation took "+time+" ms");
    }

    private void run() {
        for (int i = 1; i <= kolvo; ++i) {
            if (a == 0) {
                a = 1;
                c = a + b;
                printresult(c);
                b = c;
            } else {
                c = a + b;
                printresult(c);
                a = b;
                b = c;
            }
        }

    }

    private void printresult(int c) {
        System.out.print(c + " ");
    }

}