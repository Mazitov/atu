package homework.homework8Recursion;

import java.util.Scanner;

public class RPSLSGame {
    public static void main(String[] args) {
        RPSLSGame rpslsGame = new RPSLSGame();
        System.out.println("Добро пожаловать в игру \"Камень-Ножницы-Бумага-Ящерица-Спок\"");
        rpslsGame.run();
    }

    private void run() {
        System.out.println("Сделайте выбор и введите число: \n 0 - Камень\n 1 - Ящерица\n 2 - Спок\n 3 - Ножницы\n 4 - Бумага\n (Введите <9> чтобы завершить игру)");
        Scanner scanner = new Scanner(System.in);
        int userChoice = scanner.nextInt();
        if (userChoice == 9) {
            System.out.println("Игра завершена.");
            return;
        }
        if (userChoice > 4 || userChoice < 0) {
            System.out.println("Выбор сделан неправильно! Попробуйте еще раз.");
            run();
            return;
        }
        String viborUser = vibor(userChoice);
        System.out.println("Вы выбрали " + viborUser + ". Мммм... Прекрасный выбор!");
        int opponentValue = (int) Math.round(Math.random() * 4);
        String viborComp = vibor(opponentValue);
        System.out.println("Выбор компьютера - " + viborComp + "!");
        String result = reshenie(userChoice, opponentValue);
        System.out.println(result + "\nСпасибо за игру!");
        run();
    }

    private String vibor(int userChoice) {
        if (userChoice == 0) {
            return "Камень";
        } else if (userChoice == 1) {
            return "Ящерица";
        } else if (userChoice == 2) {
            return "Спок";
        } else if (userChoice == 3) {
            return "Ножницы";
        } else if (userChoice == 4) {
            return "Бумага";
        }
        return null;
    }

    private String reshenie(int userChoice, int opponentValue) {
        if (userChoice == opponentValue) {
            return "Ура! Победила Дружба.";
        } else {
            if (    ((userChoice - opponentValue) == -3) ||
                    ((userChoice - opponentValue) == -1) ||
                    ((userChoice - opponentValue) == 2)  ||
                    ((userChoice - opponentValue) == 4))   {
                return "Поздравляю, вы победили!";
            } else {
                return "В этот раз победил Компьютер :(";
            }
        }
    }


}


    /*
        Напишите игру Камень-Ножницы-Бумага-Ящерица-Спок.
        Игрок вводит число от 0 до 4 обозначающее один из 5 вариантов.
        Scanner scanner = new Scanner(System.in);
        int userChoice = scanner.nextInt();

        Далее компьтер случайно выбирает свой вариант
        int opponentValue = (int) Math.round(Math.random() * 4);

        Далее необходимо вывести оба выбора словам а не цифрами.
        А также написать победил ли игров или нет или была ничья.

        Игра продолжается до тех пор пока игрок не введет цифру 9.

        Ограничение: при вычислении победителя запрещено использовать switch.
        Вам необходимо найти закономерность в том, при каких выборах кто победит и написать ее в виде математической формулы.
        Используйте правила тут:
        https://bigbangtheory.fandom.com/wiki/Rock,_Paper,_Scissors,_Lizard,_Spock
     */
