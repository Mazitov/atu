package homework.homework8Recursion;

public class fibonachiRekursiya {
    /*
Необходимо вывести число фибоначчи двумя спобами, рекурсивно и в цикле.
По желанию вывести время затраченно на каждый способ.
Испльзуйте long currentTime = System.nanoTime();  для узнавания текущего времени в наносекундах
*/
    public static int a = 0; //
    public static int b = 0; //
    public static int c = 0; //
    public static int kolvo = 10; // количество цифр фибоначи
    public static int i = 1;

    public static void main(String[] args) {
        long currentTime = System.nanoTime();
        fibonachiRekursiya fibonachi = new fibonachiRekursiya();
        fibonachi.run(a, b, c);
        long currentTime2 = System.nanoTime();
        printTime((currentTime2-currentTime)/1000000);
    }

    private static void printTime(long time) {
        System.out.println("\nСalculation took "+time+" ms");
    }

    private void run(int a, int b, int c) {
        if (i < kolvo) {
            printresult(c);
            if (a == 0 & b == 0) {
                b++;
                c = a + b;
                printresult(c);
            }
            c = a + b;
            a = b;
            b = c;
            ++i;
            run(a, b, c);
        }
    }

    private void printresult(int c) {
        System.out.print(c + " ");
    }

}