package homework.homework12string;

import static java.lang.String.valueOf;

public class StringLab {

    public static void main(String[] args) {
        System.out.println(politeMethod("Dima"));

        System.out.println(makeWrapWord("Dima", "1234"));

        System.out.println(half("aaBBccDD"));

        System.out.println(endsOod("mood"));
        System.out.println(endsOod("nope"));

        System.out.println(charCounter("no no no no No!", 'n'));

        System.out.println(oddLength(1519.1));

        System.out.println(stringCounter("Java programmers love Java or Java", "java"));
    }

    //   1) Напишите вежливую функцию, которая возвращает следующую строку: "Hello Alice !"
    public static String politeMethod(String name) {
        return ("Hello " + name + "!");
    }

    /*     2) "Оберните" слово word в "обертку" wrap  Например makeWrapWord("Hello", "(())") -> ((Hello))
              makeWrapWord("Crabs", "}}{{") -> }}Crabs{{ длина wrap = 4 всегда   */
    public static String makeWrapWord(String word, String wrap) {
        return (wrap.substring(0, 2) + word + wrap.substring(2, 4));
    }

    // 3) Напишите функцию, которая возвращает первую половину строки
    public static String half(String s) {
        int halfStringLength = s.length() / 2;
        return s.substring(0, halfStringLength);
    }

    //4) Верните true, если строка оканчивается на "ood"
    public static boolean endsOod(String s) {
        if (s.endsWith("ood")) {
            return true;
        } else {
            return false;
        }
    }

    ////5) Верните количество вхождений заданного символа в заданную строку
    public static int charCounter(String s, char c) {
        int countChar = 0;
        char[] charArrayFromString = s.toCharArray();
        for (char element : charArrayFromString) {
            if (element == c) {
                countChar++;
            }
        }
        return countChar;
    }

    ////6) Преобразуйте число в строку и верните true, если количество символов в строке нечетное
    public static boolean oddLength(double d) {
        String buffer = valueOf(d);
        if (buffer.length() % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

    //7) Верните количество вхождений искомой строки в строке для поиска (игнорируя регистр)
//stringCounter("Java programmers love Java", "java") -> 2

    public static int stringCounter(String s, String searchString) {
        int counter = 0;
        int i = 0; // i и j - cчётчики. i идёт по массиву stringCheck, j по массиву keyWord
        int j = 0;
        s = s.toLowerCase();
        searchString = searchString.toLowerCase();
        char[] stringCheck = s.toCharArray();
        char[] keyWord = searchString.toCharArray();
        for (; i < stringCheck.length; i++) {
            if ((stringCheck[i] != keyWord[j]) && j != 0) {
                j = 0;
            } else if (stringCheck[i] == keyWord[j]) {
                j++;
                if (j == (keyWord.length)) {
                    counter++;
                    j = 0;
                }
            }
        }
        return counter;
    }
}











