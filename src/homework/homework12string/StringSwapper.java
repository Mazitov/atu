package homework.homework12string;
public class StringSwapper {
    public static void main(String[] args){
        //объявляем и инициализируем переменные
        int a=5,b=10;
        System.out.println(a+"  "+b); //(1)
        // пытаемся с помощью функции поменять местами значения в переменных
        swap(a,b);
        // состояние переменных неизменилось, поскольку примитивы передаются по значению!
        System.out.println(a+"  "+b);
        // Объявляем и инициализурем объекты-строки
        String sa="First",sb="Second";
        System.out.println(sa+"  "+sb);
        // Пытаемся помощью функции поменять строки местами
        swap(sa,sb);
        // Обратите внимание на результат. Строки не поменялись местами. В функцию передались копии ссылок.
        // Ссылки же для текущей функции неизменились, как и объекты на котороые они ссылаются
        System.out.println(sa+"  "+sb);

    }

    public static void swap (int x, int y)
    {
        int temp = x;
        x=y;
        y=temp;
    }

    public static void swap (String x, String y)
    {
        String temp = x;
        x=y;
        y=temp;
    }


}
