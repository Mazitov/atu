package homework.homework12string;
// создаем демонстрационный класс
class MyClass{
    public int i=0;
/*    public String toString(){
        return "This object contain "+i;
    }
 */
}

public class ToStringer {
    public static void main(String[] args){
        // создаем объект нашего демонстрационного класса
        MyClass my=new MyClass();
        // любой объект можно вывести в консоль, посколько любой объект по иерархии наследования происходит от класса Object.
        // в классе Object определен метод toString(), который вызывается неявно, когда мы пытаемся вывести объект в консоль
        // метод toString() возвращает строку в формате ИмяКласс@ХэшКод
        System.out.println(my);
        // Попробуйте раскомментировать строки 4-7
        // В них мы переопределяем метод toString()
        System.out.println(my);
    }
}


