package homework.homework12string;
public class StringFeatures {
    public static void main(String[] args){
        String a="abc";
        String b="123456789";
        //char 	charAt​(int index) Возвращает символ расположенный по указанному индексу. Строка индексируется с нуля
        System.out.println(a.charAt(2));
        //boolean 	contains​(CharSequence s) Проверяет вхождение искомой последовательности в строке
        System.out.println(a.contains("ab"));
        //boolean 	startsWith​(String prefix) Проверяет начинается ли строка на определенный префикс
        //boolean 	endsWith​(String suffix) Проверяет оканчивается ли строка на определенный суффикс
        String h="HelloWorld";
        System.out.println(a.startsWith("Hello"));
        System.out.println(a.endsWith("World"));
        //boolean 	isEmpty​() проверяет не пуста ли строка
        String emptyString="";
        System.out.println(emptyString.isEmpty());
        // Попробуйте раскомментировать следующие две строки и объяснить, почему программа аварийно завершает работу
        //String nullString=null;
        //System.out.println(nullString.isEmpty());

        //String 	substring​(int beginIndex) Вернет обрезаную строку начинающуюся с указаного символа
        //Помните, что символы в строке индексируются с 0!
        //"abc";
        System.out.println(a.substring(a.length()-1));
        //
        //String 	substring​(int beginIndex, int endIndex)
        //Аналогично указаному выше. Возвращает обрезанную строку с какого(вкл.) по какой(не вкл.) символ
        String bb="123456789";
        System.out.println(bb.substring(0,9)+" "+bb.length());
        //char[] 	toCharArray​() разбивает строку на массив символов
        String c="good";
        char[] charArray=c.toCharArray();
        String buffer="";
        for(int i=0;i<charArray.length;i++){
            if(charArray[i]!='o'){
                buffer+=charArray[i];
            }
        }
        System.out.println(buffer);
        //String 	toLowerCase​() Возвращают строку в нижнем регистре
        //String 	toUpperCase​() Возвращают строку в верхнем регистре
        String cc="good";
        System.out.println(cc.toUpperCase());
        //Помните, что стоки неизменяймы! Исходная строка не изменилась
        System.out.println(cc);
        //String 	trim​() обрезает пробелы в начале и конце строки
        System.out.println("  string ".trim());

        //Группа методов valueOf преобразует примитивы в строку
        // Обратите внимание, что для вызова этого метода объект-строка не нужна, можно указать имя класса: String s=String.ValueOf(1);
        String[] stringArray = {String.valueOf('a'),
                String.valueOf(123),
                String.valueOf(0.4),
                String.valueOf(1>0)};
        for(String s : stringArray){
            System.out.println(s);
        }
    }


}
