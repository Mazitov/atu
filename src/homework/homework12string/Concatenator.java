package homework.homework12string;

public class Concatenator {
    public static void main(String[] args){
        //Строки можно объединять с помощью метода concat(String s)
        String a="First", b="Second", c="Third";
        System.out.println(a.concat(b).concat(c));
        // Помните, что сроки неизменяемы!
        System.out.println(a);

        //Так же строки можно объединять с другими типами (в т.ч. сами строки между с собой)
        //с помощью операторов += и +. Типы данных автоматически "преобразуются" в строки с помощью неявного вызова метода toString()
        //Перед запуском программы, подумайте какие результат будут ниже
        //не забудьте о порядке вычисления
        System.out.println("" +5 + 6);   //
        System.out.println(5 + "" +6);   //
        System.out.println(5 + 6 +"");   //
        System.out.println(5 + 6);       //

        //немного о класса-обертках и зачем они нужны
        //Создаем обертку
        Integer I=new Integer(1);
        //Передаем в метод, который принимает ссылку типа Object. Помните, что любой объект унаследован от этого класса
        System.out.println(addSomeInfo(I));
        //А теперь просто передаем примитив. Казалось бы, метод ждет ссылку, и данные вызов не должен компилироваться
        //Но тут на помощь приходят классы обертки и их свойство - автоупаковка(autoboxing)
        //Примитив автоматически упаковывается в соотвествующую обертку, там где ожидается ссылка
        System.out.println(addSomeInfo(5.6));
    }

    public static String addSomeInfo(Object o){
        return "Some info "+o;
    }
}
