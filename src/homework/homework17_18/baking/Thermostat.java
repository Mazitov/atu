package homework.homework17_18.baking;

public class Thermostat {
    private Integer temperature;


    public Thermostat() {
        temperature = 25;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }
}
