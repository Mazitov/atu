package homework.homework17_18.baking;

public class GasOven extends Oven {

private GasSwitcher gasSwitcher= new GasSwitcher();

    public GasOven(Integer maxTemperature, String model, int numberOfShelves, String producer) {
        super(maxTemperature, model, numberOfShelves, producer,"Gas oven");
    }

    @Override
    public void turnOnOven() {
        super.turnOnOven();
        gasSwitcher.turnOn();
    }

    @Override
    public void turnOffOven() {
        super.turnOffOven();
        gasSwitcher.turnOff();
    }

}
