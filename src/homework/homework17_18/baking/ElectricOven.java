package homework.homework17_18.baking;

public class ElectricOven extends Oven {


    private int voltage;
    private Thermostat thermostat = new Thermostat();

    public ElectricOven(Integer maxTemperature, String model, int numberOfShelves, String producer, int voltage) {
        super(maxTemperature, model, numberOfShelves, producer, "Electric oven");
        this.voltage = voltage;
    }

    @Override
    public void changeTemperature(int temp) {
        super.changeTemperature(temp);
        thermostat.setTemperature(temp);
    }

    @Override
    public void checkTemperature() {
        super.checkTemperature();
        thermostat.getTemperature();
    }
}