package homework.homework17_18.baking;

/*ATU. Module 2, [24.07.19 10:56]
        Практика дома
        •В иерархии классов пекарь-печь из предыдущих работ сделать осмысленные конструкторы,
         примеру, у базовой печи есть производитель, модель и количество полок, у электрической
         в дополнение к этому вольтаж (220 или 380) и т.д.
        •Газовая печь должна иметь элемент – податчик газа, который связан с ней отношением композиции.
        При включении газовой печи податчик газа должен быть запущен.
        •Электрическая печь должна иметь термостат, с которым так же связана композицией.

        ATU. Module 2, [24.07.19 10:56]
        Практика дома
        •Поправить методы так, чтобы в местах, где есть копипаста из базового класса было
        обращение к методам базового класса и переопределения его (базового класса) методов.
        •Добавить функциональность выпекания пирожков.*/

abstract public class Oven {

    private Integer maxTemperature = 400;
    final private Integer minTemperature = 25;
    private final String ovenType;
    private final String model;
    private int numberOfShelves;
    private String producer;
    private Integer temperature = 25;  //текущая (и начальная) температура
    private Boolean ovenSwitch = false;
    private Boolean foodInside = false;


    public Oven(Integer maxTemperature, String model, int numberOfShelves, String producer, String ovenType) {
        this.maxTemperature = maxTemperature;
        this.model = model;
        this.numberOfShelves = numberOfShelves;
        this.producer = producer;
        this.ovenType=ovenType;
    }

    public void turnOnOven() { // включаем конкретную печь
        if (!ovenSwitch) {         // проверяем что она выключена
            ovenSwitch = true;
            System.out.print(ovenType + " is turned on.\n");
        } else {
            System.out.print(ovenType + " has been already turned on.\n");
        }
    }

    public void changeTemperature(int temp) {
        if (ovenSwitch && (temp <= maxTemperature) && (temp >= minTemperature) && (temp != temperature)) {         // можно менять температуру, только если плита включена новая температура не больше максимальной
            temperature = temp;
            System.out.print("Temperature is set to " + temp + ".\n");
        } else if (!ovenSwitch) {              // случай если плита выключена
            System.out.print("Oven is switched off. Switch it on before changing temperature.\n");
        } else if (ovenSwitch && (temp > maxTemperature)) {  // случай если выбранная температура больше максимальной
            System.out.print("Chosen temperature is higher than maximum.\n");
        } else if (ovenSwitch && (temp < minTemperature)) { // случай если выбранная температура ниже минимальной
            System.out.print("Chosen temperature is lower than minimum.\n");
        } else if (ovenSwitch && (temp == this.temperature)) { // случай, в котором новая температура совпадает со старой
            System.out.print("Oven already has this temperature (" + temp + " degrees).\n");
        }
    }

    public void turnOffOven() {  // выключаем конкретную печь
        if (ovenSwitch) {         // проверяем что она включена
            ovenSwitch = false;
            System.out.print(ovenType + " is turned off.\n");
        } else {
            System.out.print(ovenType + " has been already turned off.\n");
        }
    }

    public void placeFoodInside() {
        if (!foodInside) {
            foodInside = true;
            System.out.print("Something is placed inside.\n");
        } else {
            System.out.print("There is something inside already.\n");

        }
    }

    public void removeFood() {
        if (foodInside) {
            foodInside = false;
            System.out.print("Meal is picked out of the oven.\n");
        } else {
            System.out.print("There is no food inside of the oven.\n");
        }
    }

    public void checkTemperature() {
        System.out.println(this.temperature + " is the temperature of " + ovenType + ".\n");
    }


    public String getOvenType() {
        return ovenType;
    }

    public String getModel() {
        return model;
    }


    public String getProducer() {
        return producer;
    }


}