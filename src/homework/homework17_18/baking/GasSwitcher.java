package homework.homework17_18.baking;

public class GasSwitcher {
    private boolean switchedON = false;

    public boolean isSwitchedON() {
        return switchedON;
    }

    public void turnOn() {
        if (this.switchedON) {
            System.out.println("GasSwitcher has been already turned ON.");
        } else {
            this.switchedON = true;
            System.out.println("GasSwitcher is turned ON.");
        }
    }

    public void turnOff() {
        if (!this.switchedON) {
            System.out.println("GasSwitcher has been already turned OFF.");
        } else {
            this.switchedON = false;
            System.out.println("GasSwitcher is turned OFF.");
        }
    }

}
