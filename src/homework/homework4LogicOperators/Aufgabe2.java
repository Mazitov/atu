package homework.homework4LogicOperators;

import java.util.Scanner;

public class Aufgabe2 {
    public static void main(String[] args) {
        //Используя тернарный оператор и/или условные логические операторы, реализовать программу,
        // которая выведет результат от деления числа 100 на заданное число. Учесть возможность введения нуля,
        // в таком случае необходимо вывести максимальное число, возможное для double.


        Scanner in = new Scanner(System.in);
        System.out.print("Input a number: ");
        int num = in.nextInt();
        double max1 = 1.7976931348623157E308 ;
        System.out.println(num == 0 ? max1 : 100.10/ num);
    }
}
