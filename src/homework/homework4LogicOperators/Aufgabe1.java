package homework.homework4LogicOperators;

public class Aufgabe1 {
    public static void main(String[] args) {
        // Используя логические операции, вывести в консоль таблицы истинности для логических
        // операций И, ИЛИ, ИСКЛЮЧАЮЩЕЕ ИЛИ, НЕ, И-НЕ, ИЛИ-НЕ (последние две - отрицание
        // результата И и ИЛИ, соответственно).

        boolean boo1, boo2, boo3, boo4, boo5, boo6;
        boo1 = boo2 = boo3 = true;
        boo4 = boo5 = boo6 = false;

        // & AND
        System.out.println("True & (and) true = " + (boo1 & boo2));
        System.out.println("True & (and) false = " + (boo1 & boo4));
        System.out.println("False & (and) true = " + (boo4 & boo2));
        System.out.println("False & (and) false = " + (boo5 & boo6) + "\n");
        System.out.println();
        
        // | OR
        System.out.println("True | (or) true = " + (boo1 | boo2));
        System.out.println("True | (or) false = " + (boo1 | boo4));
        System.out.println("False | (or) true = " + (boo4 | boo2));
        System.out.println("False | (or) false = " + (boo5 | boo6) + "\n");
        System.out.println();

        // ^ OR
        System.out.println("True ^ (or) true = " + (boo1 ^ boo2));
        System.out.println("True ^ (or) false = " + (boo1 ^ boo4));
        System.out.println("False ^ (or) true = " + (boo4 ^ boo2));
        System.out.println("False ^ (or) false = " + (boo5 ^ boo6) + "\n");

        // !=  NOT EQUALS
        System.out.println("True != (not equals) true = " + (boo1 != boo2));
        System.out.println("True != (not equals) (and) false = " + (boo1 != boo4));
        System.out.println("False != (not equals) true = " + (boo4 != boo2));
        System.out.println("False != (not equals) false = " + (boo5 != boo6) + "\n");

        // ! & AND not
        System.out.println("! NOT (True & (and) true) = " + !(boo1 & boo2));
        System.out.println("! NOT (True & (and) false) = " + !(boo1 & boo4));
        System.out.println("! NOT (False & (and) true) = " + !(boo4 & boo2));
        System.out.println("! NOT (False & (and) false) = " + !(boo5 & boo6) + "\n");
        System.out.println();

        // ! | OR not
        System.out.println("! NOT (True | (or) true) = " + !(boo1 | boo2));
        System.out.println("! NOT (True | (or) false) = " + !(boo1 | boo4));
        System.out.println("! NOT (False | (or) true) = " + !(boo4 | boo2));
        System.out.println("! NOT (False | (or) false) = " + !(boo5 | boo6) + "\n");
        System.out.println();


    }
}
