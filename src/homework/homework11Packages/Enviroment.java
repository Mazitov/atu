package homework.homework11Packages;

import homework.homework11Packages.animal.*;
import homework.homework11Packages.robot.*;
import stub.Simulator;
import test.*;

public class Enviroment {
    public static void main(String[] args) {
        System.out.println(TestCase.startTestCase());
        System.out.println(TestChain.startTestChain());
        System.out.println(Simulator.startSimulator());

        Cat.sayHello();
        Dog.sayHello();
        Terminator.sayHello();
    }
}
