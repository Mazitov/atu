package homework.homework9_and_10_OOP.geometry;

import java.awt.*;


public class Rectangle extends Figure {
    Integer height, width, perimeter, areaOfFigure;
    String figureType = "Rectangle";
    Point centerPoint = new Point(0, 0);

    public Rectangle() {
    }

    Rectangle(Integer width, Integer height) {
        this.height = height;
        this.width = width;
        perimeter = 2 * (height + width);
        areaOfFigure = height * width;
        centerPoint.setLocation(height / 2, width / 2);
        System.out.println("New Rectangle with height " + height + " and width " + width + ". Coordinates of the center are " + centerPoint.getX() + ", " + centerPoint.getY() + ".");
    }

    public void getStats() {
        System.out.println("This square has height " + height + " and width " + width + ".");
        System.out.println("Coordinates of center of this rectangle are " + centerPoint.getX() + ", " + centerPoint.getY() + ".");
        System.out.println("Perimeter of this rectangle is " + perimeter + ".");
        System.out.println("Area of this rectangle is " + areaOfFigure + ".");

    }


}