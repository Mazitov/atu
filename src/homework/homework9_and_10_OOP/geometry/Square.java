package homework.homework9_and_10_OOP.geometry;

import java.awt.*;

public class Square extends Rectangle {
    String figureType = "Square";
    Point centerPoint = new Point(0, 0);
    Integer side, perimeter, areaOfFigure;


    public Square(Integer side) {
        this.side = side;
        perimeter = 4 * side;
        areaOfFigure = side * side;
        centerPoint = new Point(side / 2, side / 2);
        System.out.println("New Square with side " + side + " is created. Coordinates of the center are " + centerPoint.getX() + ", " + centerPoint.getY() + ".");
    }

    public void getStats() {
        System.out.println("Side of this square is " + side + ".");
        System.out.println("Coordinates of center of this square are " + centerPoint.getX() + ", " + centerPoint.getY() + ".");
        System.out.println("Perimeter of this square is " + perimeter + ".");
        System.out.println("Area of this square is " + areaOfFigure + ".");

    }


}
