package homework.homework9_and_10_OOP.geometry;

public class StartProgramm {
    /*  Задание 9

    Построить иерархию классов для описания плоских геометрических фигур:
    •	Круга;
    •	квадрата;
    •	прямоугольника.
    Предусмотреть следующие возможности:
    •	создание объектов;
    •	получение периметра, площади;
    •	перемещения на плоскости;
    •	поворот на заданный угол.

       Задание 10
    В иерархиях классов из предыдущей работы (геометрические фигуры, система пекарь - печь)
    заменить использованные примитивные типы их обертками.
*/

    public static void main(String[] args) {
        Circle circle = new Circle(10);
        circle.moveObject(10,5);
        circle.moveObject(2,-1);
        circle.getStats();

        System.out.println();

        Square square = new Square(15);
        square.moveObject(100,250);
        square.getStats();

        System.out.println();

        Rectangle rectangle = new Rectangle(15,25);
        rectangle.moveObject(11,-15);
        rectangle.getStats();

    }

}
