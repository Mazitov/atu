package homework.homework9_and_10_OOP.geometry;

import java.awt.*;

public class Circle extends Figure {
    public String figureType = "Circle";
    final public Double pi = 3.14;
    Point centerPoint = new Point(0, 0);
    Integer radius = 0;
    Double perimeter, areaOfFigure;


    public Circle(Integer radius) {
        this.radius = radius;
        perimeter = 2 * pi * radius;
        areaOfFigure = pi * pi * radius;
        System.out.println("New Circle with radius " + radius + " is created.");
    }



    public void getStats() {
        System.out.println("Radius of this circle is " + radius + ".");
        System.out.println("Coordinates of the center of this circle are " + centerPoint.getX() + ", " + centerPoint.getY() + ".");
        System.out.println("Perimeter of this circle is " + perimeter + ".");
        System.out.println("Area of this circle is " + areaOfFigure + ".");

    }

}
