package homework.homework9_and_10_OOP.baking;

public class GasOven extends Oven {

    public GasOven() {
        this.ovenType = "Gas Oven";
        this.maxTemperature = 600;
    }
}
