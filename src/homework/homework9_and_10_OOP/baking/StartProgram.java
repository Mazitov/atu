package homework.homework9_and_10_OOP.baking;

public class StartProgram {

    public static void main(String[] args) {
        Baker John = new Baker("John");
        Oven simpleOven = new Oven(100);
        ElectricOven electricOven = new ElectricOven();
        GasOven gasOven = new GasOven();

        
        John.turnOnOven(electricOven);
        John.changeTemperature(250, electricOven);
        John.checkTemperature(electricOven);
        John.changeTemperature(250, electricOven);
        John.placeFoodInside(electricOven);
        John.placeFoodInside(electricOven);
        John.turnOffOven(electricOven);
        John.removeFood(electricOven);

        John.turnOnOven(simpleOven);
        John.changeTemperature(250, simpleOven);
        John.checkTemperature(simpleOven);
        John.changeTemperature(250, simpleOven);
        John.placeFoodInside(simpleOven);
        John.placeFoodInside(simpleOven);
        John.turnOffOven(simpleOven);
        John.removeFood(simpleOven);
        John.removeFood(simpleOven);

        John.turnOnOven(gasOven);
        John.changeTemperature(250, gasOven);
        John.checkTemperature(gasOven);
        John.changeTemperature(250, gasOven);
        John.placeFoodInside(gasOven);
        John.placeFoodInside(gasOven);
        John.turnOffOven(gasOven);
        John.removeFood(gasOven);
        John.removeFood(gasOven);
    }
}
