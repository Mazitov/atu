package homework.homework9_and_10_OOP.baking;

public class ElectricOven extends Oven {

    public ElectricOven() {
        this.ovenType = "Electric Oven";
        this.maxTemperature = 500;
    }
}