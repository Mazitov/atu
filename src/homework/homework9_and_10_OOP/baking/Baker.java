package homework.homework9_and_10_OOP.baking;

public class Baker {
    public String nameOfBaker;

    public Baker(String nameOfBaker) {
        this.nameOfBaker = nameOfBaker;
    }

    public void turnOnOven(Oven oven) {
        System.out.println("\n" + nameOfBaker + " is trying to turn on the " + oven.ovenType + ":");
        oven.turnOnOven();
    }

    public void turnOffOven(Oven oven) {
        System.out.println("\n" + nameOfBaker + " is trying to turn off the " + oven.ovenType + ":");
        oven.turnOffOven();
    }

    public void changeTemperature(int newTemperature, Oven oven) {
        System.out.println("\n" + nameOfBaker + " is trying to change temperature of the " + oven.ovenType + " to " + newTemperature + " degrees:");
        oven.changeTemperature(newTemperature);
    }

    public void placeFoodInside(Oven oven) {
        System.out.println("\n" + nameOfBaker + " is trying to place food inside of the " + oven.ovenType + ":");
        oven.placeFoodInside();
    }

    public void removeFood(Oven oven) {
        System.out.println("\n" + nameOfBaker + " is trying to pick food out of the " + oven.ovenType + ":");
        oven.removeFood();
    }

    public void checkTemperature(Oven oven) {
        System.out.println("\n" + nameOfBaker + " is checking temperature of the " + oven.ovenType + ":");
        oven.checkTemperature();
    }

}
