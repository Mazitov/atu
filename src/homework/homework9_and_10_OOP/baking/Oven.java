package homework.homework9_and_10_OOP.baking;

public class Oven {

    public Oven() {
    }

    Oven(int temp) {
        this.temperature = temp;
    }

    public Integer maxTemperature = 400;
    final public Integer minTemperature = 25;
    public String ovenType = "Oven";
    public Integer temperature = 25;  //начальная температура
    public Boolean ovenSwitch = false;
    public Boolean foodInside = false;


    public void turnOnOven() { // включаем конкретную печь
        if (!ovenSwitch) {         // проверяем что она выключена
            ovenSwitch = true;
            System.out.print(ovenType + " is turned on.\n");
        } else {
            System.out.print(ovenType + " has been already turned on.\n");
        }
    }

    public void changeTemperature(int temp) {
        if (ovenSwitch && (temp <= maxTemperature) && (temp >= minTemperature) && (temp != temperature)) {         // можно менять температуру, только если плита включена новая температура не больше максимальной
            temperature = temp;
            System.out.print("Temperature is set to " + temp + ".\n");
        } else if (!ovenSwitch) {              // случай если плита выключена
            System.out.print("Oven is switched off. Switch it on before changing temperature.\n");
        } else if (ovenSwitch && (temp > maxTemperature)) {  // случай если выбранная температура больше максимальной
            System.out.print("Chosen temperature is higher than maximum.\n");
        } else if (ovenSwitch && (temp < minTemperature)) { // случай если выбранная температура ниже минимальной
            System.out.print("Chosen temperature is lower than minimum.\n");
        } else if (ovenSwitch && (temp == this.temperature)) { // случай, в котором новая температура совпадает со старой
            System.out.print("Oven already has this temperature (" + temp + " degrees).\n");
        }
    }

    public void turnOffOven() {  // выключаем конкретную печь
        if (ovenSwitch) {         // проверяем что она включена
            ovenSwitch = false;
            System.out.print(ovenType + " is turned off.\n");
        } else {
            System.out.print(ovenType + " has been already turned off.\n");
        }
    }

    public void placeFoodInside() {
        if (!foodInside) {
            foodInside = true;
            System.out.print("Something is placed inside.\n");
        } else {
            System.out.print("There is something inside already.\n");

        }
    }

    public void removeFood() {
        if (foodInside) {
            foodInside = false;
            System.out.print("Meal is picked out of the oven.\n");
        } else {
            System.out.print("There is no food inside of the oven.\n");
        }
    }

    public void checkTemperature() {
        System.out.println(this.temperature + " is the temperature of " + ovenType + ".\n");
    }

}