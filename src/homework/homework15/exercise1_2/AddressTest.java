package homework.homework15.exercise1_2;

public class AddressTest {
    public static void main(String[] args) {
        Address vasya = new Address("Russia", "St Petersburg", "Nevskyi Prospekt", "12a", "13", "194041");
        Address masha = new Address("Russia", "Moscow", "Leninskiy Prospekt", "120B", "113", "125101");
        Address petya = new Address("Russia", "Moscow", "Prospekt Takioto", "10B", "11");
        Address olga = new Address("", "", "", "", "");

        System.out.println(vasya.toString());
        System.out.println(masha.toString());
        System.out.println(petya.toString());

        olga.setCountry("Russia").setAppartment("14").setCity("Sochi").setHouse("49").setZipcode("454080").setStreet("Mira street");

        System.out.println(olga.toString());
    }
}
