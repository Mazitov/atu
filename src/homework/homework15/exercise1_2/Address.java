package homework.homework15.exercise1_2;
/*1. Создайте класс Адрес с полями страна, город, улица, дом, квартира, индекс.
Создайте конструктор для этого класса. Создайте второй конструктор для тех,
 кто не помнит свой индекс. Создайте пару адресов.*/

/*2. Добавьте в класс Адрес возможность изменять каждое из полей (добавьте сеттеры и геттеры).
Добавьте в сеттеры возможность вызывать сеттеры и геттеры по цепочке (пример chaining есть в файле UsingThis.java).*/


public class Address {
    private String country;
    private String city;
    private String street;
    private String house;
    private String appartment;
    private String zipcode;


    public Address(String country, String city, String street, String house, String appartment) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.house = house;
        this.appartment = appartment;
        zipcode = "N/A";
    }


    public Address(String country, String city, String street, String house, String appartment, String zipcode) {
        this(country, city, street, house, appartment);
        this.zipcode = zipcode;
    }

    @Override
    public String toString() {
        return "Address: " + zipcode + ", " + country + ", " + city + ", " + street + ", house " + house + ", appartment " + appartment + ".";
    }


    public String getCountry() {
        return country;
    }

    public Address setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Address setCity(String city) {
        this.city = city;
        return this;
    }

    public String getStreet() {
        return street;
    }

    public Address setStreet(String street) {
        this.street = street;
        return this;
    }

    public String getHouse() {
        return house;
    }

    public Address setHouse(String house) {
        this.house = house;
        return this;
    }

    public String getAppartment() {
        return appartment;
    }

    public Address setAppartment(String appartment) {
        this.appartment = appartment;
        return this;
    }

    public String getZipcode() {
        return zipcode;
    }

    public Address setZipcode(String zipcode) {
        this.zipcode = zipcode;
        return this;
    }
}
