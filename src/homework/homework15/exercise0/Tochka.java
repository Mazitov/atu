package homework.homework15.exercise0;
/*Создайте класс Точка, хранящий координаты х и у двухмерного пространства.
 Создайте конструктор для этого класса. Объявите координаты немодифицируемыми. Создайте пару точек.*/

public class Tochka {
    private final double x;
    private final double y;

    public Tochka(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {
        return "x coordinate = " + x + ", coordinate = " + y + ".";
    }

}

