package homework.homework15.exercise3_4;
/*3) Реализовать класс, позволяющий хранить информацию о человеке.
Важная информация о человеке - его фамилия, имя, отчество и дата рождения.
 Создайте конструктор. Подумайте, какие поля стоит сделать немодифицируемыми,
  какую информацию стоит вынести в конструктор, какие могут быть конструкторы.

4) Добавьте в класс Человек из задания 3 ссылку на Адрес из задания 2.
 Создайте метод, позволяющий задавать адрес человека и метод, позволяющий получать адрес.*/

import homework.homework15.exercise1_2.Address;

import java.util.ArrayList;

public class Person {
    private final String name;
    private String surname;
    private final String middleName;
    private Address address;
    private final String birthDate;
    private boolean married;
    private Person spouse;
    private int numberofChildren;
    private ArrayList<Person> children = new ArrayList<Person>();


    public Person(String name, String surname, String middleName, String birthDate) {
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.birthDate = birthDate;
        numberofChildren = 0;
    }

    public Person(String name, String surname, String middleName, String birthDate, Address address) {
        this(name, surname, middleName, birthDate);
        this.address = address;
    }

    public void setAddress(String country, String city, String street, String house, String appartment) {
        this.address = new Address(country, city, street, house, appartment);
    }

    public void setAddress(String country, String city, String street, String house, String appartment, String zipcode) {
        this.address = new Address(country, city, street, house, appartment, zipcode);
    }

    public String toString() {
        return ("Name: " + name + "\nMiddle Name: " + middleName + "\nSurname: " + surname + "\nDate of birth: "
                + birthDate + "\n" + address.toString());
    }

    public Person getSpouse() {
        return spouse;
    }

    public void setSpouse(Person spouse) {
        if (this.married == false && spouse.married == false) {
            this.spouse = spouse;
            spouse.spouse = this;
            this.married=true;
            spouse.married=true;
        } else {
            System.out.println("Person is married.");
        }
    }

    public void setChild(Person child, Person parent) {
        this.children.add(this.numberofChildren, child);
        parent.children.add(parent.numberofChildren, child);
        ++this.numberofChildren;
        ++parent.numberofChildren;
    }

    public int getNumberOfChildren() {
        return numberofChildren;
    }

    public void getDivorced() {
        this.married = false;
        spouse.married = false;
        spouse.spouse = null;
        this.spouse = null;
    }


}
