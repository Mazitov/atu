package homework.homework15.exercise3_4;

import homework.homework15.exercise1_2.Address;

public class PersonTest {

    public static void main(String[] args) {

        Person petya = new Person("Petr", "Ivanov", "Michailovich", "1991.01.01",
                new Address("Russia", "Moscow", "Prospekt Takioto", "10B", "11"));

        System.out.println(petya.toString());
    }
}
