package homework.homework15.exercise5_6;

import homework.homework15.exercise1_2.Address;
import homework.homework15.exercise3_4.Person;


public class KlassTest {

    public static void main(String[] args) {
        Klass javaSchool = new Klass(10);

        Person whiteley = new Person("Whiteley", "Watson", "Page", "01/01/1988", new Address("Russia", "Moscow", "Prospekt Takioto", "10B", "11"));
        javaSchool.addPerson(whiteley);

        Person raihan = new Person("Raihan", "Watson", "Morgan", "01/01/1988", new Address("Russia", "Moscow", "Prospekt Takioto", "10B", "11"));
        javaSchool.addPerson(raihan);

        Person krish = new Person("Krish", "Watson", "Cade", "01/01/1988", new Address("Russia", "Moscow", "Prospekt Takioto", "10B", "11"));
        javaSchool.addPerson(krish);

        Person giulia = new Person("Giulia", "Hughes", "Gilmore", "01/01/1988", new Address("Russia", "Moscow", "Prospekt Takioto", "10B", "11"));
        javaSchool.addPerson(giulia);

        Person merryn = new Person("Merryn", "Booker", "Kayne", "01/01/1988", new Address("Russia", "Moscow", "Prospekt Takioto", "10B", "11"));
        javaSchool.addPerson(merryn);

        javaSchool.addPerson(new Person("Bridget", "Stevens", "Boris", "01/01/1988", new Address("Russia", "Moscow", "Prospekt Takioto", "10B", "11")));
        javaSchool.addPerson(new Person("Ted", "Moyer", "Woodcock", "01/01/1988", new Address("Russia", "Moscow", "Prospekt Takioto", "10B", "11")));
        javaSchool.addPerson(new Person("Horace", "Watson", "Rodrigo", "01/01/1988", new Address("Russia", "Moscow", "Prospekt Takioto", "10B", "11")));
        javaSchool.addPerson(new Person("Jethro", "Lamb", "Guerrero", "01/01/1988", new Address("Russia", "Moscow", "Prospekt Takioto", "10B", "11")));
        javaSchool.addPerson(new Person("Isla-Mae", "Hines", "Martinez", "01/01/1988", new Address("Russia", "Moscow", "Prospekt Takioto", "10B", "11")));
        javaSchool.addPerson(new Person("Matias", "Hutton", "Martinez", "01/01/1988", new Address("Russia", "Moscow", "Prospekt Takioto", "10B", "11")));

        System.out.println(javaSchool); //getting info about class

        //creating a family

        whiteley.setSpouse(raihan);
        whiteley.setChild(krish, raihan);
        whiteley.setChild(giulia, merryn);

        System.out.println("Get Husbend: " + whiteley.getSpouse().toString() + "\n");
        System.out.println("Get Wife: " + raihan.getSpouse().toString() + "\n");
        System.out.println(whiteley.getNumberOfChildren());
        System.out.println(raihan.getNumberOfChildren());
    }

}
