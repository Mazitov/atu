package homework.homework15.exercise5_6;

import java.util.ArrayList;

import homework.homework15.exercise3_4.Person;

/*5) Создайте класс, описывающий группу учеников (ученик - класс Человек из задания 4).
Конструктор класса принимает на вход максимальное количество учеников.
Хранить список учеников в массиве. Реализовать метод addStudent(),
позволяющий добавить ученика, но не более количества учеников,
определенного в конструкторе. При попытке добавить больше - писать сообщение об ошибке.

6) Добавьте в класс Человек из задания 4 возможность добавить ссылку на супруга,
возможность добавить ссылки на детей (нескольких). Создайте одну или несколько семей.*/

public class Klass {


    private ArrayList<Person> klass = new ArrayList<Person>();
    private int klassMaxPerson;
    private int i = 0; //counter of pupils in class

    public Klass(int numberOfPerson) {
        if (numberOfPerson < 1 && numberOfPerson > 15) {
            klassMaxPerson = 15;
            System.out.println("Number of pupils in class is not correct.\n Set to max(15).");
        } else klassMaxPerson = numberOfPerson;
        System.out.println("New class is created. It can contain " + numberOfPerson + " pupil(s).");

    }

    public void addPerson(Person person) {
        if (this.i >= klassMaxPerson) {
            System.out.println("Class is full. Cannot add more pupils.");
            return;};
        klass.add(this.i, person);
        ++this.i;
    }

    @Override
    public String toString() {
        String s="";
        int i=0;
        for (;i<klass.size();){
            s+="\nPupil #"+(++i)+"\n"+klass.get(i-1).toString()+"\n";
        }
        return s;
    }

}
