package homework.homework15.exercise7;

/*Оперативная Память и Жесткий Диск могут содержать поля
производитель и объем.*/
public class HardDrive {

    private final String manufacturer;
    private final String type;
    private final int hdSizeGB;

    public HardDrive(String manufacturer, String type, int hdSizeGB) {
        this.manufacturer = manufacturer;
        this.type = type;
        this.hdSizeGB = hdSizeGB;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getType() {
        return type;
    }

    public int getHdSizeGB() {
        return hdSizeGB;
    }

    @Override
    public String toString() {
        return "HardDrive: {" + manufacturer + " / " + type + " / " + hdSizeGB + " GB}";
    }
}

