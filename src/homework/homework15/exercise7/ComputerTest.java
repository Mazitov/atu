package homework.homework15.exercise7;

public class ComputerTest {


    public static void main(String[] args) {
        //creating parts of computer
        final HardDrive hdd500Samsung = new HardDrive("Samsung", "HDD", 500);
        final HardDrive hdd1000Samsung = new HardDrive("Samsung", "HDD", 1000);
        final HardDrive sdd256WD = new HardDrive("WD", "SDD", 256);
        final HardDrive sdd512WD = new HardDrive("WD", "SDD", 512);

        final Motherboard gigabyteE3000 = new Motherboard("GIGABYTE", "GA-E3000N", "Intel G31", 4);
        final Motherboard gigabyteE9000 = new Motherboard("GIGABYTE", "GA-E9000N", "Intel G99", 8);
        final Motherboard esonicG31 = new Motherboard("Esonic", "G31CEL2", "Intel G19", 3);
        final Motherboard esonicG11 = new Motherboard("Esonic", "G11CL01", "Intel G08", 3);

        final Processor ryzen5 = new Processor("AMD", "Ryzen 5 2600 OEM", 3400, 6);
        final Processor sempron = new Processor("AMD", "Sempron 2650 OEM", 1450, 2);
        final Processor i3 = new Processor("Intel", "i3-8100 OEM", 3600, 4);
        final Processor i7 = new Processor("Intel", "i7-8700 OEM", 3200, 6);

        final RAM ramSamsung8 = new RAM("Samsung", "DDR3", 8);
        final RAM ramSamsung16 = new RAM("Samsung", "DDR4", 16);
        final RAM ramKingston4 = new RAM("Kingston", "DDR3", 4);
        final RAM ramKingston32 = new RAM("Kingston", "DDR4", 32);

        //creating computers
        Computer gameComputer = new Computer("Gaming computer", sdd512WD, gigabyteE9000, ryzen5, ramKingston32);
        gameComputer.setProcessor(i7);

        Computer officeComputer = new Computer("Office computer", hdd1000Samsung, esonicG11, i3, ramSamsung16);
        officeComputer.setRam(ramSamsung8);

        Computer gameComputer02 = new Computer(gameComputer, "Gaming computer model #2");
        gameComputer02.setHardDrive(sdd256WD);

        Computer officeComputer02 = new Computer(officeComputer, "New office computer");

        System.out.println(gameComputer02);
        System.out.println("\n" + officeComputer02);
    }
}
