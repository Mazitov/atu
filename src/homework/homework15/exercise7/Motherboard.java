package homework.homework15.exercise7;

/*Материнская плата: строковые поля чипсет, производитель, количество портов USB.*/

public class Motherboard {

    private final String manufacturer;
    private final String modelName;
    private final String chipset;
    private final int numberOfUSBports;


    public Motherboard(String manufacturer, String modelName, String chipset, int numberOfUSBports) {
        this.manufacturer = manufacturer;
        this.chipset = chipset;
        this.numberOfUSBports = numberOfUSBports;
        this.modelName = modelName;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getChipset() {
        return chipset;
    }

    public int getNumberOfUSBports() {
        return numberOfUSBports;
    }

    @Override
    public String toString() {
        return "Motherboard: {" + manufacturer + " " + modelName + " / chipset " + chipset + " / " + numberOfUSBports + " USB-ports}";
    }
}
