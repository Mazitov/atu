package homework.homework15.exercise7;

/*Оперативная Память и Жесткий Диск могут содержать поля
производитель и объем.*/
public class RAM {

    private final String manufacturer;
    private final String moduleFormat;
    private final int ramSize;

    public RAM(String manufacturer, String moduleFormat, int ramSize) {
        this.manufacturer = manufacturer;
        this.moduleFormat = moduleFormat;
        this.ramSize = ramSize;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModuleFormat() {
        return moduleFormat;
    }

    public int getRamSize() {
        return ramSize;
    }

    @Override
    public String toString() {
        return "RAM: {" + manufacturer + " / " + moduleFormat + " / " + ramSize + " GB}";
    }
}
