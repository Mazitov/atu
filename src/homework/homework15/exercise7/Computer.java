package homework.homework15.exercise7;

/*7) Реализовать класс Компьютер, позволяющий хранить ссылки на классы: Процессор, Материнская плата,
 Оперативная Память, Жесткий диск и строковое поле имя, как-то обозначающее конфигурацию
 (например “Игровой”, “Рабочий”, или “Модель 1”). Каждый из внутренних классов должен
содержать два или простых больше полей, например:
Материнская плата: строковые поля чипсет, производитель, количество портов USB. Процессор -
поля тактовая частота, производитель. Оперативная Память и Жесткий Диск могут содержать поля
производитель и объем. Значение полей не важно, наша задача - научиться пользоваться конструкторами.
Создайте в каждом из классов хотя бы один конструктор. Подумайте, какие из полей можно сделать
 немодифицируемыми. В классе “Компьютер” также создайте конструктор копирования - конструктор,
позволяющий создать точно такой же экземпляр, как уже созданный ранее (скопировать все поля).
По желанию, добавьте другие компоненты компьютера (блок питания, видеокарту и т. п.,
добавьте логику конфигурирования). Создайте пару конфигураций.

7*) Задание со звездочкой, по желанию - добавьте в класс “Компьютер” метод toString(),
 возвращающий строку с конфигурацией компьютера в понятном текстовом формате (см. пример в файле CopyCat.java).*/

public class Computer {

    private final String nameComputer;
    private HardDrive hardDrive;
    private Motherboard motherboard;
    private Processor processor;
    private RAM ram;

    public Computer(String nameComputer, HardDrive hardDrive, Motherboard motherboard, Processor processor, RAM ram) {
        this.nameComputer = nameComputer;
        this.hardDrive = hardDrive;
        this.motherboard = motherboard;
        this.processor = processor;
        this.ram = ram;
    }

    public Computer(Computer anotherComp, String nameComputer) {
        this.nameComputer = nameComputer;
        this.ram = anotherComp.ram;
        this.motherboard = anotherComp.motherboard;
        this.processor = anotherComp.processor;
        this.hardDrive = anotherComp.hardDrive;
    }

    public HardDrive getHardDrive() {
        return hardDrive;
    }

    public void setHardDrive(HardDrive hardDrive) {
        this.hardDrive = hardDrive;
    }

    public Motherboard getMotherboard() {
        return motherboard;
    }

    public void setMotherboard(Motherboard motherboard) {
        this.motherboard = motherboard;
    }

    public Processor getProcessor() {
        return processor;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
    }

    public RAM getRam() {
        return ram;
    }

    public void setRam(RAM ram) {
        this.ram = ram;
    }

    public String getNameComputer() {
        return nameComputer;
    }

    @Override
    public String toString() {
        return "\n" + nameComputer + ":" +
                "\n" + motherboard +
                "\n" + processor +
                "\n" + ram +
                "\n" + hardDrive;
    }
}
