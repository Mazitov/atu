package homework.homework15.exercise7;
/*Процессор -поля тактовая частота, производитель.*/

public class Processor {


    private final String manufacturer;
    private final String modelName;
    private final int frequencyHZ;
    private final int numberOfCores;


    public Processor(String manufacturer, String modelName, int frequencyHZ, int numberOfCores) {
        this.manufacturer = manufacturer;
        this.frequencyHZ = frequencyHZ;
        this.numberOfCores = numberOfCores;
        this.modelName = modelName;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getFrequencyHZ() {
        return frequencyHZ;
    }

    public int getNumberOfCores() {
        return numberOfCores;
    }

    public String getModelName() {
        return modelName;
    }

    @Override
    public String toString() {
        return "Processor: {" + manufacturer + " " + modelName + " / " + frequencyHZ + "HZ / " + numberOfCores + " cores}";
    }
}

